using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.Plugin.Messenger;
using MvvmCross.ViewModels;
using Zedo.Core.ViewModels;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core
{
    public class CoreApp : MvxApplication
    {
        public static string OfflineDatabaseName => "OfflineStorage";
        public override void Initialize()
        {
            AutoMapperConfiguration.Initialize();

            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            Mvx.LazyConstructAndRegisterSingleton<IMvxMessenger, MvxMessengerHub>();

            NotificationCenter.Initialize();

            var identityService = Mvx.Resolve<IIdentityService>();
            var currentIdentity = identityService.CurrentIdentity();
            if (currentIdentity == null)
            {
                RegisterCustomAppStart<CustomStart<LoginViewModel>>();
            }
            else if(currentIdentity.IsActivated)
            {
                RegisterCustomAppStart<CustomStart<HomeRootViewModel>>();
            }
            else if(currentIdentity.IsActivationRequested)
            {
                RegisterCustomAppStart<CustomStart<RegisterActivationViewModel>>();
            }
            else
            {
                identityService.LogOut();
                RegisterCustomAppStart<CustomStart<LoginViewModel>>();
            }
        }
    }
}
