using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace Zedo.Core
{
    public class CustomStart<TViewModel> : MvxAppStart<TViewModel>
        where TViewModel : IMvxViewModel
    {
        public CustomStart(IMvxApplication application, IMvxNavigationService navigationService) : base(application, navigationService)
        {
//            application.Startup(true);
        }

        protected override void NavigateToFirstViewModel(object hint)
        {
            NavigationService.Navigate<TViewModel>();
        }
    }
}