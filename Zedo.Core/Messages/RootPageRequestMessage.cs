﻿using MvvmCross.Plugin.Messenger;

namespace Zedo.Core.Messages
{
    public class RootPageRequestMessage
        : MvxMessage
    {
        public RootPageRequestMessage(object sender)
            : base(sender)
        {
            
        }
    }
}