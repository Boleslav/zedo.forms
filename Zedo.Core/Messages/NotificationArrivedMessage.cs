﻿using MvvmCross.Plugin.Messenger;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.Messages
{
    public class NotificationArrivedMessage : MvxMessage
    {
        public NotificationItem Notification { get; }

        public NotificationArrivedMessage(object sender, NotificationItem notification) : base(sender)
        {
            Notification = notification;
        }
    }
}