﻿using Zedo.Core.Data.Repository;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.ViewModels
{
    public interface IRepositoryService
    {
        LiteDBRepository<NotificationItem> Notifications { get; }
    }
}