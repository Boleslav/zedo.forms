﻿using System.Collections.Generic;

namespace Zedo.Core.ViewModels.Portal
{
    public class StoreItem : StoreCategoryItem
    {
        public string Title2 { get; set; }
        public int OffersCount { get; set; }
        public int PromotionType { get; set; }

        public bool HasOffers => OffersCount > 0;
        public bool IsOfferMode { get; set; }

        public StoreItem(int id, string title, string image) : base(id, title, image)
        {
        }

        public string StoreName { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Floor { get; set; }
//        public string StoreLogo { get; set; }
        public string Website { get; set; }
        public int? LocationID { get; set; }
        public string LocationName { get; set; }
        public string OffersTitle { get; set; }
        public string GoogleMapUrl { get; set; }

        public IEnumerable<StoreProductCategory> ProductCategories { get; set; }
        public IEnumerable<WorkingHours> WorkingHours { get; set; }
        public IEnumerable<StoreOffer> Offers { get; set; }

        public IEnumerable<string> Gallery { get; set; }
        public bool IsFavorite { get; set; }

        public string Address { get; set; }

        public bool ShowQrCodeByDefault { get; set; } = true;

    }
}