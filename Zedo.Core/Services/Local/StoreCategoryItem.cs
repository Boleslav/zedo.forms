﻿namespace Zedo.Core.ViewModels.Portal
{
    public class StoreCategoryItem
    {
        public StoreCategoryItem(long id, string title, string image)
        {
            Id = id;
            Title = title;
            Image = image;
        }

        public long Id { get; set; }
        public string Image { get; set; }
        public string Title { get; set; }
    }
}