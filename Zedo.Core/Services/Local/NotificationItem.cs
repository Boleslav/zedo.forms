﻿using System;
using Zedo.Core.Data.Repository;

namespace Zedo.Core.ViewModels.Portal
{
    public class NotificationItem : IDbLiteEntity
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
    }
}