﻿namespace Zedo.Core.ViewModels.Portal
{
    public class Country : StoreCategoryItem
    {
        public Country(long id, string title) : base(id, title, null)
        {
        }

        public string Code { get; set; }
    }
}