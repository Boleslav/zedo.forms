﻿namespace Zedo.Core.ViewModels.Portal
{
    public class StoreProductCategory : StoreCategoryItem
    {
        public StoreProductCategory(int id, string title, string image) : base(id, title, image)
        {
        }
    }
}