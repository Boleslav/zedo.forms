﻿using Zedo.Core.ViewModels.Api;

namespace Zedo.Core.ViewModels.Portal
{
    public class AdItem
    {
        public string Image { get; set; }
        public MobilePages Type { get; set; }
        public string Link { get; set; }
    }
}