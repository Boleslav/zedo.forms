﻿using Zedo.Core.Data.Repository;

namespace Zedo.Core.ViewModels
{
    public interface IIdentityService
    {
        void LogOut();
        void SignIn(UserSettings user);
        void UpdateSignIn(UserSettings user);

        UserSettings CurrentIdentity();
        bool IsSignedIn { get; }
    }
}