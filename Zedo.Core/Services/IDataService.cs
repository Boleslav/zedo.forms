using System.Collections.Generic;
using System.Threading.Tasks;
using Zedo.Core.Data.Repository;
using Zedo.Core.ViewModels.Api;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.ViewModels
{
    public interface IDataService
    {
        Task<IEnumerable<Country>> GetCountriesAvailable();
        IEnumerable<SelectSimpleItem> GetGendersAvailable();

        Task<IEnumerable<StoreCategoryItem>> GetMallsAsync(string search = null);
        Task<IEnumerable<StoreCategoryItem>> GetProductTypesAsync();
        Task<IEnumerable<StoreCategoryItem>> GetStoreLocationsAsync();
        Task<IEnumerable<StoreCategoryItem>> GetBrandsAsync();

        Task<IEnumerable<StoreItem>> GetStoresByCategoryAsync(long[] categoryIds, StoreCategoryTypes categoryType);
        Task<IEnumerable<StoreItem>> GetFavoriteStoresAsync();

        Task<IEnumerable<StoreOffer>> GetOffersAsync(bool showNewOnly = false);
        Task<IEnumerable<StoreOffer>> GetRedeemedOffersAsync();

        Task<RedeemedCoupon> RedeemOffer(string pinCode, StoreOffer offer);
        Task<RedeemedCoupon> GetRedeemedCoupon(long offerId);

        Task<StoreItem> GetStoreAsync(long storeId);
        Task<bool> SetStoreFavorite(long storeId, bool isFavorite);

        Task<UserSettings> Login(string email, string password);
        Task<UserSettings> RegisterUser(UserSettings customer);
        Task<bool> SaveUser(UserSettings customer);

        Task<bool> ActivateUser(long userId, string activationCode);
        Task<UserSettings> ResendUserActivationCode(long userId);
//        Task<IEnumerable<NotificationItem>> GetNotificationsAsync();

        Task LoadCommonData();
        Task<AdItem> GetAd(MobilePages sectionType);

        void AddNotification(NotificationItem item);
        IEnumerable<NotificationItem> GetNotifications();
    }
}