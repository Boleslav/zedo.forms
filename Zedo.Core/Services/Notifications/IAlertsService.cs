﻿namespace LawPilot.Mobile.Core.Services
{
    public interface IAlertsService
    {
        void StartLoadingProgress(string title = null);
        void StopLoadingProgress();

        void ShowToast(string message);
    }
}