using System;
using System.Diagnostics;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Push;
using MvvmCross;
using Zedo.Core.ViewModels;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core
{
    public class NotificationCenter
    {
        public static void Initialize()
        {
            if (!AppCenter.Configured)
            {
                return;
                Push.PushNotificationReceived += (sender, e) =>
                {
                    // Add the notification message and title to the message
                    var summary = $"Push notification received:" +
                                  $"\n\tNotification title: {e.Title}" +
                                  $"\n\tMessage: {e.Message}";

                    // If there is custom data associated with the notification,
                    // print the entries
                    if (e.CustomData != null)
                    {
                        summary += "\n\tCustom data:\n";
                        foreach (var key in e.CustomData.Keys)
                        {
                            summary += $"\t\t{key} : {e.CustomData[key]}\n";
                        }
                    }

                    var notification = new NotificationItem
                    {
                        Body = summary,
                        Date = DateTime.Now
                    };
                    Mvx.Resolve<IDataService>().AddNotification(notification);
                    // Send the notification summary to debug output
                    Debug.WriteLine(summary);
                };
            }

            var appId = "{Your App Secret}";
            AppCenter.Start(appId, typeof(Push));
        }
    }
}