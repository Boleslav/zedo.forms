﻿using Acr.UserDialogs;

namespace LawPilot.Mobile.Core.Services
{
    internal class AlertsService : IAlertsService
    {
        public void StartLoadingProgress(string title)
        {
            UserDialogs.Instance.ShowLoading(title ?? "");
        }

        public void StopLoadingProgress()
        {
            UserDialogs.Instance.HideLoading();
        }

        public void ShowToast(string message)
        {
            UserDialogs.Instance.Toast("    " + message);
        }
    }
}