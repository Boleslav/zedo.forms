﻿using MvvmCross;
using Zedo.Core.Data.Repository;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.ViewModels
{
    public class RepositoryService : IRepositoryService
    {
        public LiteDBRepository<NotificationItem> Notifications { get; }

        public RepositoryService()
        {
            Notifications = Mvx.Resolve<ILiteDbService>().GetRepository<NotificationItem>();
        }
    }
}