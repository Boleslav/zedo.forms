﻿namespace Zedo.Core.ViewModels.Api
{
    public interface IApiAccessService
    {
        IDataAccessPolicies Policies { get; }
    }
}