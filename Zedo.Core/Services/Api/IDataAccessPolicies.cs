﻿using System.Net.Http;
using Forkbench.MobileConnectivity.Core.Builder;

namespace Zedo.Core.ViewModels.Api
{
    public interface IDataAccessPolicies
    {
        IDataAccessPolicy<HttpClient> Raw { get; }
        IDataAccessPolicy<IApi> Default { get; }
    }
}