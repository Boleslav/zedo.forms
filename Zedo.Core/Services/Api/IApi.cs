﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Forkbench.MobileConnectivity.Core;
using Refit;
using Zedo.Core.Models.Api;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.ViewModels.Api
{
    public interface IApi
    {
        [Get("/malls")]
        Task<GenericResult<IEnumerable<MallsView>>> GetMalls();

        [Get("/productCategories")]
        Task<GenericResult<IEnumerable<ProductCategoriesView>>> GetProductCategories();

        [Get("/brands")]
        Task<GenericResult<IEnumerable<BrandsView>>> GetBrands();

        [Get("/locations")]
        Task<GenericResult<IEnumerable<Locations>>> GetLocations();

        [Get("/stores/{id}")]
        Task<GenericResult<StoresView>> GetStore(long id);
        [Get("/stores/customer/{id}/{customerId}")]
        Task<GenericResult<StoresView>> GetStoreForCustomer(long id, long customerId);
        [Get("/favorites/customer/{customerId}")]
        Task<GenericResult<IEnumerable<StoresView>>> GetCustomerFavoriteStores(long customerId);

        [Get("/stores/mall")]
        Task<GenericResult<IEnumerable<StoresView>>> GetStoresByMall([Query(CollectionFormat.Multi)]long[] id);
        [Get("/stores/brand")]
        Task<GenericResult<IEnumerable<StoresView>>> GetStoresByBrand([Query(CollectionFormat.Multi)]long[] id);
        [Get("/stores/location")]
        Task<GenericResult<IEnumerable<StoresView>>> GetStoresByLocation([Query(CollectionFormat.Multi)]long[] id);
        [Get("/stores/productCategory")]
        Task<GenericResult<IEnumerable<StoresView>>> GetStoresByProductType([Query(CollectionFormat.Multi)]long[] id);

        //        [Get("/stores/{id}")]
        //        Task<StoresView> GetStore(int storeId);
        [Post("/favorites/customer")]
        Task<GenericResult<bool>> SetStoreAsFavorite([Body(BodySerializationMethod.UrlEncoded)]Dictionary<string, object> data);
        [Delete("/favorites/customer")]
        Task<GenericResult<bool>> SetStoreAsNotFavorite([Body(BodySerializationMethod.UrlEncoded)]Dictionary<string, object> data);

        [Get("/customers/login/{email}/{password}")]
        Task<GenericResult<Customer>> Login(string email, string password);
        [Get("/customers/recover/{email}")]
        Task<GenericResult<Customer>> Recover(string email);
        [Get("/customers/pinCode/{id}/{pinCode}")]
        Task<GenericResult<Customer>> GetPinCode(long id, string pinCode);
        [Put("/customers/changePin")]
        Task<GenericResult<Customer>> ChangePin([Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, object> data);
        [Put("/Customers/ActivatePhone/{id}")]
        Task<GenericResult<Customer>> ActivatePhone(long id, [Body(BodySerializationMethod.UrlEncoded)] Dictionary<string, object> data);
        [Put("/customers/resendPhoneCode/{id}")]
        Task<GenericResult<Customer>> ResendPhoneCode(long id);
        [Post("/customers/register")]
        Task<GenericResult<Customer>> Register([Body] Customer customer);
        [Put("/customers/save")]
        Task<GenericResult<Customer>> Save([Body] Customer customer);
        
        [Get("/countries")]
        Task<GenericResult<IEnumerable<CountryView>>> GetCountries();

        [Get("/offers")]
        Task<GenericResult<IEnumerable<StoresOffersView>>> GetAllOffers();
        [Get("/offers/new")]
        Task<GenericResult<IEnumerable<StoresOffersView>>> GetNewOffers();
        [Get("/offers/redeemed/{userId}")]
        Task<GenericResult<IEnumerable<StoresOffersView>>> GetRedeemedOffers(long userId);

        [Post("/offers/coupons")]
        Task<GenericResult<RedeemCoupons>> RedeemOffer([Body]RedeemCoupons coupon);
        [Get("/offers/coupons/{userId}/{offerId}")]
        Task<GenericResult<RedeemCoupons>> GetRedeemedCoupon(long userId, long offerId);

//        [Get("/notfications")]
//        Task<GenericResult<IEnumerable<NotificationItem>>> GetNotifications();

        [Get("/mobileads")]
        Task<GenericResult<IEnumerable<MobileAdsView>>> GetAds();
    }
}