﻿namespace Zedo.Core.ViewModels.Api
{
    public class ApiAccessService : IApiAccessService
    {
        public IDataAccessPolicies Policies { get; }

        public ApiAccessService()
        {
            Policies = new DataAccessPolicies();
        }
    }
}