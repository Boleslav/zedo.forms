﻿using System;
using System.Net.Http;
using Forkbench.MobileConnectivity.Core;
using Forkbench.MobileConnectivity.Core.Builder;
using Refit;

namespace Zedo.Core.ViewModels.Api
{
    public class DataAccessPolicies : IDataAccessPolicies
    {
        public DataAccessPolicies()
        {
            Default = new DataPoliciesBuilder<IApi>()
                .Priority(DataPoliciesPriorityOptions.Default)
                .LimitUpTo(5)
                .MaxTimeout(7)
                .FallbackHttpExceptionHandler<IApiResponse>(ex => new ApiResponseGeneric(false))
                .BuildPolicy(handler =>
                {
                    var client = new HttpClient(handler)
                    {
                        BaseAddress = new Uri(DataService.ApiBaseUrl)
                    };

                    return RestService.For<IApi>(client);
                });

            Raw = new DataPoliciesBuilder<HttpClient>()
                .Priority(DataPoliciesPriorityOptions.Default)
                .LimitUpTo(5)
                .MaxTimeout(10)
                .FallbackHttpExceptionHandler<IApiResponse>(ex => new ApiResponseGeneric(false))
                .BuildPolicy();
        }

        public IDataAccessPolicy<IApi> Default { get; }
        public IDataAccessPolicy<HttpClient> Raw { get; }
    }
}