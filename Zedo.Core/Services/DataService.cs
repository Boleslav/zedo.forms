using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Akavache.Sqlite3.Internal;
using Forkbench.MobileConnectivity.Core;
using MvvmCross;
using MvvmCross.Plugin.Messenger;
using Refit;
using Zedo.Core.Data.Repository;
using Zedo.Core.Messages;
using Zedo.Core.Models.Api;
using Zedo.Core.ViewModels.Api;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.ViewModels
{
    public class DataService : IDataService
    {
        private readonly IRepositoryService _repositories;
        private readonly IDataAccessPolicies _dataAccess;

//        public static string ApiBaseUrl => "https://459c3117.ngrok.io/";
//        public static string ApiBaseUrl => "https://12b84f1b.ngrok.io/api";
//        public static string ApiBaseUrl => "http://192.168.11.9:8081/api";
        public static string ApiBaseUrl => "http://35.224.150.210:2016/api";

        public DataService(IRepositoryService repositories)
        {
            _repositories = repositories;

            _dataAccess = new DataAccessPolicies();
            _api = RestService.For<IApi>(ApiBaseUrl);

        }

        private async Task<GenericResult<T>> ProcessApi<T>(Func<IApi, Task<GenericResult<T>>> func)
        {
            try
            {
                return await func(_api);
            }
            catch (Exception e)
            {
                var type = typeof(T);
                Type resultType;
                var isEnumerable = type.IsSubclassOf(typeof(IEnumerable));
                if (isEnumerable)
                {
                    resultType = type.GetGenericArguments()[0];
                }
                else
                {
                    resultType = type;
                }

                var returningType = !isEnumerable ? resultType:
                        typeof(Array).MakeGenericType(resultType);
                
                var returningGenericType = typeof(GenericResult<>).MakeGenericType(returningType);
                return (GenericResult<T>)Activator.CreateInstance(returningGenericType);
            }
            /*Func<IApi, Task<object>> f = async (api) =>
            {
                var t = (Task)func(api);
                await t;
                var r = ((dynamic)t).Result;
                return r;
            };

            var res = await _dataAccess.Default.ExecuteAsync(api => f(api), CancellationToken.None);
            if (res is T)
            {
                return (T)res;
            }
            else
            {
                return (T)Activator.CreateInstance(type);
            }
*/

        }


        public async Task<IEnumerable<Country>> GetCountriesAvailable()
        {
            var result = await ProcessApi(api => api.GetCountries());
            var countries = (result.Value ?? new CountryView[0])
                .Select(x => new Country(x.CountryID, x.CountryNameEn){Code = x.CountryCode}).ToArray();
            return countries;
        }

        public IEnumerable<SelectSimpleItem> GetGendersAvailable()
        {
            return new[]
            {
                new SelectSimpleItem(1, "Male"),
                new SelectSimpleItem(2, "Female"),
            };
        }

/*
        public async Task<IEnumerable<StoreCategoryItem>> GetMallsAsync()
        {
            return new List<StoreCategoryItem>()
            {
                new StoreCategoryItem(1, "Qatar Mall", "mall_qatar"),
                new StoreCategoryItem(1, "Royal Plaza Mall", "mall_royal_plaza"),
                new StoreCategoryItem(1, "Villaggio Mall", "mall_villaggio"),
                new StoreCategoryItem(1, "Doha Festival City", "mall_doha_festival_city"),
                new StoreCategoryItem(1, "The Mall", "mall_the_mall"),
                new StoreCategoryItem(1, "Alhazm Mall", "mall_alhazm"),
                new StoreCategoryItem(1, "Tawar Mall", "mall_tawar"),
            };
        }
*/


        public async Task<IEnumerable<StoreOffer>> GetOffersAsync(bool showNewOnly = false)
        {
            var result = await ProcessApi(api => showNewOnly ? api.GetNewOffers() : api.GetAllOffers());
            var offers = (result.Value ?? new StoresOffersView[0])
                .Select(x => new StoreOffer(x.StoreCouponID, x.CouponTitle, x.CouponImage)
                {
                    PromoTypeTitle = x.ProductName,//"On All/Selected Items/Redeemed",
                    IsExpired = false, // ??
                    ProductName = x.ProductName,
                    StoreImage = x.StoreLogo,
                    StoreId = x.StoreId,
                    From = x.CouponFrom,
                    To = x.CouponTo,
                    IsHot = !x.IsYearly,
                    IsYearly = x.IsYearly,
                    IsRedeemed = x.IsRedeemed,
                    DaysLeftFormatted = x.DaysLeft
                }).ToArray();
            return offers;
        }

        private readonly List<RedeemedCoupon> _redeemedCouponsCache = new List<RedeemedCoupon>();
        private IApi _api;

        public async Task<RedeemedCoupon> GetRedeemedCoupon(long offerId)
        {
            var identityService = Mvx.Resolve<IIdentityService>();
            var userId = identityService.IsSignedIn ? identityService.CurrentIdentity().CustomerId : 0;
            if (userId == 0)
            {
                throw new Exception("User not signed in to redeem offer.");
            }

            var couponFound = _redeemedCouponsCache.FirstOrDefault(x => x.OfferId == offerId && x.UserId == userId);
            if (couponFound != null)
            {
                return couponFound;
            }

            var result = await ProcessApi(api => api.GetRedeemedCoupon(userId, offerId));
            var couponDto = result.Value;
            if (couponDto == null)
            {
                return null;
            }

            var coupon = new RedeemedCoupon()
            {
                Id = couponDto.RedeemID,
                Code = couponDto.Ref,
                OfferId = couponDto.FkStoreOfferID,
                UserId = userId,
                RedeemedAt = couponDto.RedeemDate
            };

            AddRedeemedCouponToCache(coupon);

            return coupon;
        }

        private void AddRedeemedCouponToCache(RedeemedCoupon coupon)
        {
            var couponFound = _redeemedCouponsCache.FirstOrDefault(x => x.OfferId == coupon.OfferId && x.UserId == coupon.UserId);
            if (couponFound == null)
            {
                _redeemedCouponsCache.Add(coupon);
            }
        }

        public async Task<RedeemedCoupon> RedeemOffer(string pinCode, StoreOffer offer)
        {
            if (string.IsNullOrWhiteSpace(pinCode))
            {
                throw new Exception("Pin cannot be empty for redemption.");
            }

            var identityService = Mvx.Resolve<IIdentityService>();
            var userId = identityService.IsSignedIn ? identityService.CurrentIdentity().CustomerId : 0;
            if (userId == 0)
            {
                throw new Exception("User not signe in to redeem offer.");
            }

            var couponToCreate = new RedeemCoupons
            {
                FkCustomerID = userId,
                FkStoreOfferID = offer.Id,
                PinStore = pinCode,
            };

            var result = await ProcessApi(api => api.RedeemOffer(couponToCreate));
            var couponDto = result.Value;
            if (couponDto == null)
            {
                return null;
            }

            var coupon = new RedeemedCoupon()
            {
                Id = couponDto.RedeemID,
                Code = couponDto.Ref,
                OfferId = couponDto.FkStoreOfferID,
                UserId = userId,
                RedeemedAt = couponDto.RedeemDate
            };

            AddRedeemedCouponToCache(coupon);

            return coupon;
        }

        public async Task<IEnumerable<StoreOffer>> GetRedeemedOffersAsync()
        {
            var identityService = Mvx.Resolve<IIdentityService>();
            var userId = identityService.IsSignedIn ? identityService.CurrentIdentity().CustomerId : 0;
            if (userId == 0)
            {
                return new StoreOffer[0];
            }

            var result = await ProcessApi(api => api.GetRedeemedOffers(userId));
            var offers = (result.Value ?? new StoresOffersView[0])
                .Select(x => new StoreOffer(x.StoreCouponID, x.CouponTitle, x.CouponImage)
                {
                    PromoTypeTitle = x.ProductName,//"On All/Selected Items/Redeemed",
                    IsExpired = false, // ??
                    StoreImage = x.StoreLogo,
                    ProductName = x.ProductName,
                    StoreId = x.StoreId,
                    From = x.CouponFrom,
                    To = x.CouponTo,
                    IsHot = !x.IsYearly,
                    IsYearly = x.IsYearly,
                    IsRedeemed = x.IsRedeemed,
                    DaysLeftFormatted = x.DaysLeft
                }).ToArray();
            return offers;
        }

        public async Task<IEnumerable<StoreCategoryItem>> GetMallsAsync(string search)
        {
//            var client = new HttpClient(handler)
//            {
//                BaseAddress = new Uri(DataService.ApiBaseUrl)
//            };

//            var result = await RestService.For<IApi>(ApiBaseUrl).GetMalls();
            var result = await ProcessApi(api => api.GetMalls());
            var malls = (result.Value ?? new MallsView[0])
//            var malls = (result.Value ?? new MallsView[0])
                .Select(x => new StoreCategoryItem(x.MallID, x.MallName, x.MallLogo))
                .ToArray();
            return malls;
        }

        public async Task<IEnumerable<StoreCategoryItem>> GetProductTypesAsync()
        {
            var result = await ProcessApi(api => api.GetProductCategories());
            return (result.Value ?? new ProductCategoriesView[0])
                .Select(x => new StoreCategoryItem(x.ProductCategoryID, x.ProductCategoryName, x.ProductCategoryImage))
                .ToArray();
        }

        public async Task<IEnumerable<StoreCategoryItem>> GetStoreLocationsAsync()
        {
            var result = await ProcessApi(api => api.GetLocations());
            return (result.Value ?? new Locations[0])
                .Where(x => !x.IsRemoved).Select(x => new StoreCategoryItem(x.LocationID, x.NameEn, "logo_location"))
                .ToArray();
        }

        public async Task<IEnumerable<StoreCategoryItem>> GetBrandsAsync()
        {
            var result = await ProcessApi(api => api.GetBrands());
            return (result.Value ?? new BrandsView[0])
                .Select(x => new StoreCategoryItem(x.BrandID, x.BrandName, x.BrandLogo))
                .ToArray();
        }

        public async Task<IEnumerable<StoreItem>> GetStoresByCategoryAsync(long[] categoryIds, StoreCategoryTypes categoryType)
        {
            var result = await ProcessApi(api =>
            {
                if (categoryType == StoreCategoryTypes.Mall)
                {
                    return api.GetStoresByMall(categoryIds);
                }
                if (categoryType == StoreCategoryTypes.Location)
                {
                    return api.GetStoresByLocation(categoryIds);
                }
                if (categoryType == StoreCategoryTypes.ProductType)
                {
                    return api.GetStoresByProductType(categoryIds);
                }
                if (categoryType == StoreCategoryTypes.Brand)
                {
                    return api.GetStoresByBrand(categoryIds);
                }
                throw new NotImplementedException();
            });

            var stores = (result.Value ?? new StoresView[0])
                .Select(x => new StoreItem(x.StoreID, x.StoreName, x.StoreLogo)
                {
                    OffersCount = x.OffersCount ?? 0,
//                    Title2 = x.StoreName
                })
                .ToArray();

            return stores;

/*
            return new List<StoreItem>
            {
                new StoreItem(0, "Bugatti", "store_bugatti") { Title2 = "Women's clothing", OffersCount = 2},
                new StoreItem(1, "Centrepoint", "store_centrepoint") { Title2 = "Women's clothing", OffersCount = 2},
                new StoreItem(2, "Debenhams", "store_debenhams") { Title2 = "Women's clothing", OffersCount = 2},
                new StoreItem(3, "Salam Stores", "store_salam") { Title2 = "Women's clothing", OffersCount = 2},

            };
*/
        }

        public async Task<IEnumerable<StoreItem>> GetFavoriteStoresAsync()
        {
            var identity = Mvx.Resolve<IIdentityService>().CurrentIdentity();
            if (identity?.IsActivated != true)
            {
                return new List<StoreItem>();
            }

            var result = await ProcessApi(api => api.GetCustomerFavoriteStores(identity.CustomerId));

            var stores = (result.Value ?? new StoresView[0])
                .Select(x => new StoreItem(x.StoreID, x.StoreName, x.StoreLogo)
                {
                    OffersCount = x.OffersCount ?? 0,
                    //                    Title2 = x.StoreName
                })
                .ToArray();

            return stores;
        }

        public async Task<StoreItem> GetStoreAsync(long storeId)
        {
            long? customerId = 0;

            var result = await ProcessApi(api => (customerId ?? 0) == 0 ? api.GetStore(storeId) : api.GetStoreForCustomer(storeId, (long)customerId));
            var st = result.Value;
            if (st == null)
            {
                return null;
            }

            var store = new StoreItem(st.StoreID, st.StoreName, st.StoreLogo)
            {
                Description = st.Description,
                Email = st.Email,
                Floor = st.Floor,
                GoogleMapUrl = st.GoogleMapUrl,
                LocationID = st.LocationID,
                LocationName = st.LocationName,
                Phone = st.Phone,
                Website = st.Website,
                OffersCount = st.OffersCount ?? 0,
                ProductCategories = st.Products.Select(x => new StoreProductCategory(x.StoreProductCategoryID, x.ProductName, x.ProductImage)).ToArray(),
                WorkingHours = st.WorkingHours.Select(x => new WorkingHours(x.StoreWorkTimeID, x.WorkingHours)).ToArray(),
                Offers = st.Offers.Select(x => new StoreOffer(x.StoreCouponID, x.CouponTitle, x.CouponImage)
                {
                    PromoTypeTitle = x.ProductName,//"On All/Selected Items/Redeemed",
                    IsExpired = false, // ??
                    ProductName = x.ProductName,
                    StoreImage = x.StoreLogo,
                    StoreId = x.StoreId == 0 ? (int)storeId : x.StoreId,
                    From = x.CouponFrom,
                    To = x.CouponTo,
                    IsHot = !x.IsYearly,
                    IsYearly = x.IsYearly,
                    IsRedeemed = x.IsRedeemed,
                    DaysLeftFormatted = x.DaysLeft
                }).ToArray(),

                Gallery = st.Gallery.ToArray(),
                IsFavorite = st.IsCustomerFavorite
            };

            var favorites = await GetFavoriteStoresAsync();
            store.IsFavorite = favorites.Any(x => x.Id == store.Id);
            return store;
        }

        public async Task<bool> SetStoreFavorite(long storeId, bool isFavorite)
        {
            var identity = Mvx.Resolve<IIdentityService>().CurrentIdentity();
            if (identity == null)
            {
                return false;
            }
            var data = new Dictionary<string, object>()
            {
                { "customerId", identity.CustomerId },
                { "storeId", storeId }
            };

            GenericResult<bool> result;
            if (isFavorite)
            {
                result = await ProcessApi(api => api.SetStoreAsFavorite(data));
            }
            else
            {
                result = await ProcessApi(api => api.SetStoreAsNotFavorite(data));
            }

            return result.Value;
        }

        public async Task<UserSettings> Login(string email, string password)
        {
            var result = await ProcessApi(api => api.Login(email, password));
            if (result.IsSuccess)
            {
                var u = result.Value;
                return new UserSettings()
                {
                    CustomerId = u.CustomerID,

                    CustomerName = u.CustomerName,
                    Email = u.Email,
                    Phone = u.Phone,
                    Pin = u.Pin,
                    IsPhoneActive = u.IsPhoneActive,
                    PhoneCode = u.PhoneCode,
                    ActivationCode = u.ActivationCode,
                    Gender = u.Gender,
                    IsNewsSubscriber = u.IsNewsSubscriber,
                    CountryId = u.FKCountryID,

                    IsActivated = true
                };
            }
            else
            {
                return null;
            }
        }

        public async Task<UserSettings> RegisterUser(UserSettings user)
        {
            var customer = new Customer()
            {
                Password = user.Password,
                Email = user.Email,
                Phone = user.Phone,
                Gender = user.Gender,
                IsNewsSubscriber = user.IsNewsSubscriber,
                CustomerName = user.CustomerName,
                IsPhoneActive = user.IsPhoneActive,
                Pin = user.Pin,
//                PhoneCode = user.PhoneCode,
//                ActivationCode = user.ActivationCode,
                CustomerID = 0,//user.CustomerId,
                FKCountryID = user.CountryId,
//                IsActive = user.isa
            };
            var result = await ProcessApi(api => api.Register(customer));
            if (result.IsSuccess)
            {
                var u = result.Value;
                return new UserSettings
                {
                    CustomerId = u.CustomerID,
                    CustomerName = u.CustomerName,
                    Email = u.Email,
                    Phone = u.Phone,
                    Pin = u.Pin,
                    IsPhoneActive = u.IsPhoneActive,
                    PhoneCode = u.PhoneCode,
                    ActivationCode = u.ActivationCode,
                    Gender = u.Gender,
                    IsNewsSubscriber = u.IsNewsSubscriber,
                    CountryId = u.FKCountryID
                };
            }
            else
            {
                return null;
            }
        }

        public async Task<bool> SaveUser(UserSettings user)
        {
            var customer = new Customer()
            {
                Password = user.Password,
                Email = user.Email,
                Phone = user.Phone,
                Gender = user.Gender,
                IsNewsSubscriber = user.IsNewsSubscriber,
                CustomerName = user.CustomerName,

                CustomerID = user.CustomerId,
                FKCountryID = user.CountryId,
            };

            var result = await ProcessApi(api => api.Save(customer));
            return result.Value != null;
        }

        public async Task<bool> ActivateUser(long userId, string activationCode)
        {
            var result = await ProcessApi(api => api.ActivatePhone(userId, new Dictionary<string, object>(){{"", activationCode}}) /*"\"=" + activationCode + "\""*/);
            return result.IsSuccess; // possibly update the user with the results sent
        }

        public async Task<UserSettings> ResendUserActivationCode(long userId)
        {
            var result = await ProcessApi(api => api.ResendPhoneCode(userId));
            if (result.IsSuccess)
            {
                var u = result.Value;
                return new UserSettings()
                {
                    CustomerName = u.CustomerName,
                    Email = u.Email,
                    Phone = u.Phone,
                    Pin = u.Pin,
                    IsPhoneActive = u.IsPhoneActive,
                    PhoneCode = u.PhoneCode,
                    ActivationCode = u.ActivationCode,
                    Gender = u.Gender,
                    IsNewsSubscriber = u.IsNewsSubscriber,
                    CountryId = u.FKCountryID
                };
            }
            else
            {
                return null;
            }
        }

/*
        public async Task<IEnumerable<NotificationItem>> GetNotificationsAsync()
        {
            var result = await ProcessApi(api => api.GetNotifications());
            if (result.IsSuccess)
            {
                return result.Value.ToArray();
            }
            else
            {
                return new NotificationItem[0];
            }             
        }
*/

        private readonly AsyncLock _adsAsyncLock = new AsyncLock();
        private IEnumerable<MobileAdsView> _adsCache = null;
        public async Task LoadCommonData()
        {
            await LoadAds();
        }

        private async Task<IEnumerable<MobileAdsView>> LoadAds()
        {
            using (await _adsAsyncLock.LockAsync())
            {
                if (_adsCache == null)
                {
                    var result = await ProcessApi(api => api.GetAds());
                    _adsCache = result?.Value;
                }
            }

            return _adsCache;
        }

        public async Task<AdItem> GetAd(MobilePages sectionType)
        {
            var ads = await LoadAds();
            var result = ads?.FirstOrDefault(x => x.MobilePageID == sectionType);

            return result == null ? null : new AdItem
            {
                Image = result.AdImage,
                Type = result.MobilePageID,
                Link = result.AdLink
            };
        }

        public void AddNotification(NotificationItem item)
        {
            CleanNotifications();
            _repositories.Notifications.UpSertItem(item);

            var _messenger = Mvx.Resolve<IMvxMessenger>();
            var message = new NotificationArrivedMessage(this, item);
            _messenger.Publish(message);  
        }

        public IEnumerable<NotificationItem> GetNotifications()
        {
            CleanNotifications();
            return _repositories.Notifications.ReadAllItems().ToArray();
        }

        private readonly object _notificationsCleaningLock = new object();
        private void CleanNotifications()
        {
            lock(_notificationsCleaningLock)
            {
                var cleaningDate = DateTime.Now.AddDays(-7);
                var oldNotifications = _repositories.Notifications.ReadAllItems().Where(x => x.Date < cleaningDate);
                foreach (var n in oldNotifications)
                {
                    _repositories.Notifications.DeleteItem(n);
                }
            }
        }
    }
}