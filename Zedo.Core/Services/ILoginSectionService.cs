﻿namespace Zedo.Core.ViewModels.Local
{
    public interface ILoginSectionService
    {
        bool IsLoginSectionModal { get; set; }
    }
}