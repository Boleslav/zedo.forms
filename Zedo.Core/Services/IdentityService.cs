﻿using System.Linq;
using MvvmCross;
using Zedo.Core.Data.Repository;

namespace Zedo.Core.ViewModels
{
    public class IdentityService : IIdentityService
    {
        private LiteDBRepository<UserSettings> _settingsRepository;
        private LiteDBRepository<UserSettings> SettingsRepository => _settingsRepository = _settingsRepository ?? Mvx.Resolve<ILiteDbService>().GetRepository<UserSettings>();

        public void LogOut()
        {
            SettingsRepository.DeleteAll();
        }

        public void SignIn(UserSettings user)
        {
            SettingsRepository.DeleteAll();
            SettingsRepository.UpSertItem(user);
        }

        public UserSettings CurrentIdentity()
        {
            return SettingsRepository.ReadAllItems().FirstOrDefault();
        }

        public void UpdateSignIn(UserSettings user)
        {
            SettingsRepository.UpSertItem(user);
        }

        public bool IsSignedIn
        {
            get
            {
                var currentIdentity = CurrentIdentity();
                return currentIdentity != null && currentIdentity.IsActivated;
            }
        }
    }
}