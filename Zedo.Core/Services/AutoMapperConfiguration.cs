using AutoMapper;
using Zedo.Core.Data.Repository;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core
{
    public static class AutoMapperConfiguration
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<UserSettings, UserSettings>();

                cfg.CreateMap<FilterOptions, FilterOptions>();
                cfg.CreateMap<FilterOptionGroup, FilterOptionGroup>();
                cfg.CreateMap<FilterOptionItem, FilterOptionItem>();
            });
        }
    }
}