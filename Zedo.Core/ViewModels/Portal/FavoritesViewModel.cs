using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MvvmCross;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using Xamarin.Forms.Internals;

namespace Zedo.Core.ViewModels.Portal
{
    public class FavoritesViewModel : BaseViewModel
    {
        private StoreItem _selectedItem;

        public IEnumerable<StoreItem> Items
        {
            get => _items;
            set => SetProperty(ref _items, value);
        }

        public FavoritesViewModel()
        {

        }

        public StoreItem SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                SetProperty(ref _selectedItem, value);
                if (_selectedItem != null)
                {
                    NavigateStore(_selectedItem);
                }
            }
        }

        private void NavigateStore(StoreItem store)
        {
            var request = new StoreDetailsViewModelRequest(store.Id, store.Title);
            NavigationService.Navigate<StoreDetailsViewModel, StoreDetailsViewModelRequest>(request);
        }

        public override async void Prepare()
        {

        }

        public override async void ViewAppearing()
        {
            base.ViewAppearing();

            if (IdentityService.IsSignedIn)
            {
                // to start loading during the view navigation
                await LoadFavorites();
            }
        }

        private bool firstTimeInitialized = false;
        public override async void ViewAppeared()
        {
            base.ViewAppeared();

            // todo: handle situation when the same viewmodel created twice
            if (!IdentityService.IsSignedIn && !firstTimeInitialized)
            {
                firstTimeInitialized = true;

                // to ask for signin in after the navigation finished
                var isSignedIn = await EnsureSigningIn();
                if (isSignedIn)
                {
                    await LoadFavorites();
                }
            }
        }

        Task<IEnumerable<StoreItem>> favsTask = null;
        private IEnumerable<StoreItem> _items;

        private async Task LoadFavorites()
        {
            var items = await LoadFavoritesEx();
            favsTask = null;
            Items = items;
        }

        private Task<IEnumerable<StoreItem>> LoadFavoritesEx()
        {
            return favsTask = favsTask ?? DataService.GetFavoriteStoresAsync();
        }
    }

}