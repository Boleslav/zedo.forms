

using System.Collections.Generic;
using System.Threading.Tasks;
using MvvmCross.Plugin.Messenger;
using MvvmCross.ViewModels;
using Zedo.Core.Messages;

namespace Zedo.Core.ViewModels.Portal
{
    public class NotificationsViewModel : BaseViewModel
    {
        private readonly IMvxMessenger _messenger;
        private NotificationItem _selectedItem;
        private IEnumerable<NotificationItem> _items;
        private MvxSubscriptionToken _onNewMessageSubscription;

        public IEnumerable<NotificationItem> Items
        {
            get => _items;
            set => SetProperty(ref _items, value);
        }

        public NotificationsViewModel(IMvxMessenger messenger)
        {
            _messenger = messenger;
            _onNewMessageSubscription = _messenger.SubscribeOnMainThread<RootPageRequestMessage>(message => RefreshMessages());
        }

        private void RefreshMessages()
        {
            LoadNotifications();
        }

        public NotificationItem SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                SetProperty(ref _selectedItem, value);
                if (_selectedItem != null)
                {
                    NavigateItem(_selectedItem);
                }
            }
        }

        private void NavigateItem(NotificationItem store)
        {
//            var request = new StoreDetailsViewModelRequest(store.Id, store.Title);
//            NavigationService.Navigate<StoreDetailsViewModel, StoreDetailsViewModelRequest>(request);
        }

        public override async void Prepare()
        {

        }

        public override async void ViewAppearing()
        {
            base.ViewAppearing();

            await LoadNotifications();
        }

        private async Task LoadNotifications()
        {
            Items = DataService.GetNotifications();
        }

    }
}