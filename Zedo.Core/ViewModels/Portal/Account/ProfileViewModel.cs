

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LawPilot.Mobile.Core.Services;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.Presenters.Hints;
using MvvmCross.ViewModels;
using Zedo.Core.Data.Repository;

namespace Zedo.Core.ViewModels.Portal
{
    public class ProfileViewModel : BaseViewModel
    {
        private readonly IIdentityService _identityService;
        private readonly IAlertsService _alertsService;
        private UserSettings _user;
        private bool _isNewsSubscriber;
        private string _name;

        private SelectSimpleItem<Country> _nationalitySelected;
        private SelectSimpleItem _genderSelected;

        private SelectSimpleItem<Country>[] _countriesCache;

        public IMvxCommand GoChangePassword => new MvxCommand(async () => await ProcessChangePassword());
        public IMvxCommand GoLogout => new MvxCommand(async () => await ProcessLogout());
        public IMvxCommand GoSave => new MvxCommand(async () => await ProcessSave());

        public IMvxCommand GoNationality => new MvxCommand(NavigateNationality);
        public IMvxCommand GoGender => new MvxCommand(NavigateGender);

        private async Task ProcessSave()
        {
            _alertsService.StartLoadingProgress("");
            try
            {
                var userCopy = Mapper.Map<UserSettings>(User);

                userCopy.IsNewsSubscriber = IsNewsSubscriber;
                userCopy.CustomerName = Name;

                var succeded = await DataService.SaveUser(userCopy);
                if (succeded)
                {
                    _alertsService.ShowToast("Saved successfully");
                }
                else
                {
                    _alertsService.ShowToast("Error occured when saving");
                }
            }
            finally
            {
                _alertsService.StopLoadingProgress();
            }
        }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public SelectSimpleItem<Country> NationalitySelected
        {
            get => _nationalitySelected;
            set => SetProperty(ref _nationalitySelected, value);
        }

        public SelectSimpleItem GenderSelected
        {
            get => _genderSelected;
            set => SetProperty(ref _genderSelected, value);
        }

        public IMvxCommand GoOffers => new MvxCommand(() => IsNewsSubscriber = !IsNewsSubscriber);

        public ProfileViewModel(IIdentityService identityService, IAlertsService alertsService)
        {
            _identityService = identityService;
            _alertsService = alertsService;

            User = _identityService.CurrentIdentity();
        }

        public UserSettings User
        {
            get => _user;
            set => SetProperty(ref _user, value);
        }

        public bool IsNewsSubscriber
        {
            get => _isNewsSubscriber;
            set => SetProperty(ref _isNewsSubscriber, value);
        }

        private async Task ProcessLogout()
        {
            _identityService.LogOut();

            var presentation = new MvxBundle(new Dictionary<string, string> { { "NavigationMode", "ReplaceRoot" } });
            await NavigationService
                .Navigate<LoginViewModel, LoginSectionRequest>(null, presentation);
        }

        private async Task ProcessChangePassword()
        {
            await NavigationService.Navigate<ChangePasswordViewModel>();
        }

        public override async void Prepare()
        {
            base.Prepare();

            var countries = await DataService.GetCountriesAvailable();
            _countriesCache = countries.Select(x => new SelectSimpleItem<Country>(x.Id, x.Title, false, x)).ToArray();

            NationalitySelected = _countriesCache.FirstOrDefault(x => x.Id == User.CountryId);
            GenderSelected = DataService.GetGendersAvailable().FirstOrDefault(x => x.Id == User.Gender);

            IsNewsSubscriber = User.IsNewsSubscriber;
            Name = User.CustomerName;
            // todo: to be added, but needs additional verification of the number entered
//            Phone = User.Phone;
        }

        private async void NavigateNationality()
        {
            var model = new SelectSimpleRequest()
            {
                Header = "Nationality",
                Title = "SELECT YOUR NATIONALITY",
                Items = _countriesCache,
                IsSearchable = true
            };

            var result = await NavigationService.Navigate<SettingsSelectSimpleViewModel, SelectSimpleRequest, SelectSimpleItem>(model);
            if (result != null)
            {
                NationalitySelected = (SelectSimpleItem<Country>)result;
            }
        }

        private async void NavigateGender()
        {
            var model = new SelectSimpleRequest()
            {
                Header = "Gender",
                Title = "SELECT YOUR GENDER",
                Items = DataService.GetGendersAvailable()
            };

            var result = await NavigationService.Navigate<SettingsSelectSimpleViewModel, SelectSimpleRequest, SelectSimpleItem>(model);
            if (result != null)
            {
                GenderSelected = result;
            }
        }
    }
}