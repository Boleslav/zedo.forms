using System.Collections.Generic;
using System.Linq;
using MvvmCross;
using MvvmCross.ViewModels;
using Xamarin.Forms.Internals;

namespace Zedo.Core.ViewModels.Portal
{
    public class RedemptionHistoryViewModel : BaseViewModel
    {
        private List<StoreOffer> _items;

        public List<StoreOffer> Items
        {
            get => _items;
            set => SetProperty(ref _items, value);
        }

        public RedemptionHistoryViewModel()
        {

        }

        public override async void Prepare()
        {
            base.Prepare();

            var items = await DataService.GetRedeemedOffersAsync();
            Items = items.ToList();// .Where(x => x.RedemptionDate < today minus six month);
        }
    }
}