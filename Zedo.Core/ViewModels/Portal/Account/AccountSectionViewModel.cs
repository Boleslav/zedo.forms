



using System.Collections.Generic;
using System.Threading.Tasks;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace Zedo.Core.ViewModels.Portal
{
    public class AccountSectionViewModel : BaseViewModel
    {
        public IMvxCommand GoRedemptionHistory => new MvxCommand(async () => await ProcessRedemptionHistory());

        private async Task ProcessRedemptionHistory()
        {
            var isSignedIn = await EnsureSigningIn();
            if (isSignedIn)
            {
                await NavigationService.Navigate<RedemptionHistoryViewModel>();
            }
        }

        public IMvxCommand GoProfile => new MvxCommand(async () => await ProcessProfile());

        private async Task ProcessProfile()
        {
            var isSignedIn = await EnsureSigningIn();
            if (isSignedIn)
            {
                await NavigationService.Navigate<ProfileViewModel>();
            }
        }

        public IMvxCommand GoInfo => new MvxCommand(() => NavigationService.Navigate<InfoViewModel>());
    }
}