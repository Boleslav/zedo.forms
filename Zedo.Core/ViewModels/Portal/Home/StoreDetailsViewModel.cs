﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using Xamarin.Forms;

namespace Zedo.Core.ViewModels.Portal
{
    public class StoreDetailsViewModel : BaseViewModel<StoreDetailsViewModelRequest>
    {
        private StoreOffer _selectedOffer;
        private IEnumerable<StoreCategoryItem> _productsAvailable;
        private bool _isFavorite;
        private StoreItem _store;
        private string _title;
        private string[] _pictures;
        private IEnumerable<StoreOffer> _offers;
        private IEnumerable<StoreOffer> _offersRedeemed;
        private IEnumerable<WorkingHours> _workingHours;
        private long? _storeId;

        public IEnumerable<StoreOffer> Offers
        {
            get { return _offers; }
            set { SetProperty(ref _offers, value); }
        }

        public IEnumerable<StoreOffer> OffersRedeemed
        {
            get { return _offersRedeemed; }
            set { SetProperty(ref _offersRedeemed, value); }
        }

        public IMvxCommand GoFavoriteState { get; set; }

        public IMvxCommand GoCall { get; set; }
        public IMvxCommand GoEmail { get; set; }
        public IMvxCommand GoMap { get; set; }

        public IMvxCommand GoOffer { get; set; }

        public StoreDetailsViewModel()
        {
            GoFavoriteState = new MvxCommand(async () => await ProcessFavoriteSwitch());

            GoOffer = new MvxCommand<StoreOffer>(NavigateOffer);
            GoCall = new MvxCommand(() => Device.OpenUri(new Uri("tel:" + Store?.Phone)), () => !string.IsNullOrWhiteSpace(Store?.Phone));
            GoEmail = new MvxCommand(() => Device.OpenUri(new Uri("mailto:" + Store?.Email)), () => !string.IsNullOrWhiteSpace(Store?.Email));
            GoMap = new MvxCommand(() => Device.OpenUri(new Uri(Store?.GoogleMapUrl)), () => !string.IsNullOrWhiteSpace(Store?.GoogleMapUrl));
            /*{
                var name = "Mall Name".Replace("&", "and"); // var name = Uri.EscapeUriString(place.Name);
                var loc = $"{"25.301"},{"51.555"}";
                var addr = Uri.EscapeUriString(Store?.Address ?? "373 INTERCHANGE JAHHANIYA، Ar-Rayyan, Qatar");

                string request;
                if (Device.RuntimePlatform == Device.Android)
                {
                    request = $"geo:0,0?q={(string.IsNullOrWhiteSpace(addr) ? loc : addr)}({name})";
                }
                else
                {
                    request = $"http://maps.apple.com/maps?q={name.Replace(' ', '+')}&sll={loc}";
                }

                Device.OpenUri(new Uri(request));
            });*/

            IsBusy = true;
        }

        private async Task ProcessFavoriteSwitch()
        {
            if (IsFavorite)
            {
                // todo: show progress
                await DataService.SetStoreFavorite(Store.Id, false);
                IsFavorite = false;
            }
            else
            {
                // trying to sign in
                var isSignedIn = await EnsureSigningIn();
                if (isSignedIn)
                {
                    // todo: show progress
                    await DataService.SetStoreFavorite(Store.Id, true);
                    IsFavorite = true;
                }
            }
        }

        public bool IsFavorite
        {
            get { return _isFavorite; }
            set { SetProperty(ref _isFavorite, value); }
        }

        public string[] Pictures
        {
            get { return _pictures; }
            set { SetProperty(ref _pictures, value); }
        }

        
        public int CurrentPicturePosition { get; set; }

        public StoreOffer SelectedOffer
        {
            get { return _selectedOffer; }
            set
            {
                SetProperty(ref _selectedOffer, value);
                if (_selectedOffer != null)
                {
                    NavigateOffer(_selectedOffer);
                }
            }
        }

        private async void NavigateOffer(StoreOffer storeOffer)
        {
            var isSignedIn = await EnsureSigningIn();
            if (!isSignedIn)
            {
                return;
            }

            var request = new StoreDetailsRequest()
            {
                Offer = storeOffer,
                Store = Store
            };
            await NavigationService.Navigate<OfferDetailsViewModel, StoreDetailsRequest>(request);
        }


        public override async void Prepare(StoreDetailsViewModelRequest request)
        {
            Title = request.StoreTitle;

            try
            {
                _storeId = request.StoreId;
                Store = await DataService.GetStoreAsync(request.StoreId);
                if (Store == null)
                {
                    await NavigationService.Close(this);
                    return;
                }
                Title = Store.Title;

                Pictures = Store.Gallery.ToArray();
                IsFavorite = Store.IsFavorite;

                WorkingHours = Store.WorkingHours.ToArray();
                ProductsAvailable = Store.ProductCategories.ToList();

                SetupOffers(Store);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private void SetupOffers(StoreItem store)
        {
            if (store == null)
            {
                // todo: explore how to eliminate occasional error
                return;
            }

            Offers = store.Offers.Where(x => !x.IsRedeemed).ToArray();
            OffersRedeemed = store.Offers.Where(x => x.IsRedeemed).ToArray();
        }

        public override async void ViewAppearing()
        {
            base.ViewAppearing();

            // refreshing offers on new entering to the page (possibly, after the offer redemption)
            if (_storeId != null)
            {
                var store = await DataService.GetStoreAsync((long)_storeId);
                SetupOffers(store);
            }
        }

        public IEnumerable<WorkingHours> WorkingHours
        {
            get => _workingHours;
            set => SetProperty(ref _workingHours, value);
        }

        public StoreItem Store
        {
            get { return _store; }
            set { SetProperty(ref _store, value); }
        }

        public IEnumerable<StoreCategoryItem> ProductsAvailable
        {
            get => _productsAvailable;
            set => SetProperty(ref _productsAvailable, value);
        }

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }
    }
}