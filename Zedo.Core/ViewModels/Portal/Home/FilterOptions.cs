﻿using System.Collections.Generic;
using MvvmCross.Commands;
using Xamarin.Forms.Internals;

namespace Zedo.Core.ViewModels.Portal
{
    public class FilterOptions
    {
        public IEnumerable<FilterOptionGroup> Groups { get; set; }

        public IMvxCommand GoClear => new MvxCommand(ProcessClearFilters);

        private void ProcessClearFilters()
        {
            Groups.ForEach(x => x.Items.ForEach(y => y.SetInitial()));
        }

        public FilterOptions()
        {
            
        }
    }

    public class FilterOptionGroup
    {
        public string Title { get; set; }
        public ICollection<FilterOptionItem> Items { get; set; }
    }
}