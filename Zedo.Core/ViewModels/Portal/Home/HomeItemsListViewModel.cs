﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Zedo.Core.ViewModels.Api;

namespace Zedo.Core.ViewModels.Portal
{
    public class HomeItemsListViewModel : BaseViewModel<HomeItemsListViewModelRequest>
    {
        private IMvxCommand _goSelectItem;
        private MvxCommand _goSelectAll;
        private string _itemsCountTitle;
        private IEnumerable<StoreCategoryItem> _items;
        private string _title;
        private string _titleAll;
        private bool _isLoading;
        private AdItem _adBanner;

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public string TitleAll
        {
            get { return _titleAll; }
            set { SetProperty(ref _titleAll, value); }
        }

        public string ItemsCountTitle
        {
            get { return _itemsCountTitle; }
            set { SetProperty(ref _itemsCountTitle, value); }
        }

//        public string Image { get; set; }

        public bool IsLoading
        {
            get { return _isLoading; }
            set { SetProperty(ref _isLoading, value); }
        }

        public AdItem AdBanner
        {
            get => _adBanner;
            set => SetProperty(ref _adBanner, value);
        }

        public StoreCategoryTypes StoreCategoryType { get; set; } = StoreCategoryTypes.General;

        public int ItemsCount => Items?.Count() ?? 0;
        public IEnumerable<StoreCategoryItem> Items
        {
            get { return _items; }
            set
            {
                SetProperty(ref _items, value);
                RaisePropertyChanged(() => ItemsCount);
            }
        }

        public IMvxCommand GoSelectItem
        {
            get { return _goSelectItem = _goSelectItem ?? new MvxCommand<StoreCategoryItem>(NavigateItem); }
        }
        public IMvxCommand GoSelectAll
        {
            get { return _goSelectAll = _goSelectAll ?? new MvxCommand(NavigateAll); }
        }

        private void NavigateAll()
        {
            NavigateItem(null);
        }

        private void NavigateItem(StoreCategoryItem item)
        {
            var request = new StoresListViewModelRequest()
            {
                RequestingInstanceId = item?.Id ?? 0,
                RequestingInstanceTitle = item?.Title,
                RequestingInstanceType = StoreCategoryType
            };

            NavigationService.Navigate<StoresListViewModel, StoresListViewModelRequest>(request);
        }

        public override Task Initialize()
        {
            return base.Initialize();
        }

        public override async void Start()
        {
            base.Start();
        }

        public override async void Prepare(HomeItemsListViewModelRequest parameter)
        {
            StoreCategoryType = parameter.StoreCategoryType;

//            Image = "home_category_malls.jpg";
            var titleAll = "";
            var itemsCountTitle = "";

            Func<Task<IEnumerable<StoreCategoryItem>>> dataLoader = null;
            MobilePages? adPageType = null;
            if (StoreCategoryType == StoreCategoryTypes.Mall)
            {
                titleAll = itemsCountTitle = Title = "Malls";
//                Image = "home_category_malls.jpg";

                adPageType = MobilePages.Malls;
                dataLoader = () => Mvx.Resolve<IDataService>().GetMallsAsync();
            }
            else if (StoreCategoryType == StoreCategoryTypes.ProductType)
            {
                Title = "Categories";
                titleAll = "Products";
                itemsCountTitle = "Categories";
//                Image = "home_category_products.jpg";

                adPageType = MobilePages.Stores;
                dataLoader = () => Mvx.Resolve<IDataService>().GetProductTypesAsync();
            }
            else if (StoreCategoryType == StoreCategoryTypes.Location)
            {
                titleAll = itemsCountTitle = Title = "Locations";
//                Image = "home_category_locations.jpg";

                adPageType = MobilePages.Locations;
                dataLoader = () => Mvx.Resolve<IDataService>().GetStoreLocationsAsync();
            }
            else if (StoreCategoryType == StoreCategoryTypes.Brand)
            {
                titleAll = itemsCountTitle = Title = "Brands";
//                Image = "home_category_brands.jpg";

                adPageType = MobilePages.Brands;
                dataLoader = () => Mvx.Resolve<IDataService>().GetBrandsAsync();
            }
            else
            {
                Items = new StoreCategoryItem[0];
                titleAll = itemsCountTitle = Title = "[ NOT IMPLEMENTED ]";
            }

            TitleAll = "ALL " + titleAll.ToUpper();
            ItemsCountTitle = itemsCountTitle.ToUpper();

            try
            {
                if (adPageType != null)
                {
                    IsLoading = true;
                    AdBanner = await DataService.GetAd(adPageType.Value);
                }

                if (dataLoader != null)
                {
                    IsLoading = true;

                    var items = await dataLoader();
                    if (Device.RuntimePlatform == Device.Android)
                    {
                        // slight delay for smooth navigation during navigation rendering
                        await Task.Delay(50);
                    }
                    Items = items;

//                Items.ForEach(x =>
//                {
//                    x.Title = x.Title;//.ToUpper();
//                    x.Image += x.Image.EndsWith(".png") ? "" : ".png";
//                });
                }

            }
            finally
            {
                IsLoading = false;
            }
        }
    }
}