﻿using MvvmCross.Commands;
using MvvmCross.ViewModels;

namespace Zedo.Core.ViewModels.Portal
{
    public class FilterOptionItem : MvxViewModel
    {
        private bool _isActive;
        private readonly bool _isActiveInitial;
        public string Title { get; set; }
        public long Id { get; set; }

        public bool IsActive
        {
            get => _isActive;
            set => SetProperty(ref _isActive, value);
        }

        public IMvxCommand FilterChanged => new MvxCommand(() => IsActive = !IsActive);

        public FilterOptionItem(bool isActive)
        {
            _isActive = isActive;
            _isActiveInitial = isActive;
        }

        public void SetInitial()
        {
            IsActive = _isActiveInitial;
        }
    }
}