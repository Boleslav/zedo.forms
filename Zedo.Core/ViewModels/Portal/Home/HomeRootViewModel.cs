using System.Collections.Generic;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.Logging;
using MvvmCross.ViewModels;

namespace Zedo.Core.ViewModels.Portal
{
    public class HomeRootViewModel : MvxViewModel
    {
        public HomeRootViewModel()
        {
            ShowInitialViewModelsCommand = new MvxAsyncCommand(ShowInitialViewModels);
//            ShowTabsRootBCommand = new MvxAsyncCommand(async () => await NavigationService.Navigate<TabsRootBViewModel>());
        }

        public IMvxAsyncCommand ShowInitialViewModelsCommand { get; private set; }

//        public IMvxAsyncCommand ShowTabsRootBCommand { get; private set; }

        private async Task ShowInitialViewModels()
        {
            var tasks = new List<Task>();
            tasks.Add(NavigationService.Navigate<HomeViewModel>());
            tasks.Add(NavigationService.Navigate<NotificationsViewModel>());
            tasks.Add(NavigationService.Navigate<FavoritesViewModel>());
            tasks.Add(NavigationService.Navigate<AccountSectionViewModel>());
            await Task.WhenAll(tasks);
        }

        private int _itemIndex;

        public int ItemIndex
        {
            get { return _itemIndex; }
            set
            {
                if (_itemIndex == value) return;
                _itemIndex = value;
                Log.Trace("Tab item changed to {0}", _itemIndex.ToString());
                RaisePropertyChanged(() => ItemIndex);
            }
        }
    }
}