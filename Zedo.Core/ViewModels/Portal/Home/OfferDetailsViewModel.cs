﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using Xamarin.Forms.Internals;

namespace Zedo.Core.ViewModels.Portal
{
    public class OfferDetailsViewModel: BaseViewModel<StoreDetailsRequest>
    {
        private bool _isRedemptionComplete;
        private StoreOffer _storeOffer;
        private StoreItem _store;
        private RedeemedCoupon _coupon;
        private string _couponCodeFormatted;
        private string _couponCode;
        private string _pinCode;
        private bool _isPinValid;

        public OfferDetailsViewModel()
        {
            Coupon = new RedeemedCoupon
            {
                Code = Guid.NewGuid().ToString("N")
            };
            IsBusy = true;
        }

        public StoreOffer StoreOffer
        {
            get => _storeOffer;
            private set => SetProperty(ref _storeOffer, value);
        }

        public StoreItem Store
        {
            get => _store;
            set => SetProperty(ref _store, value);
        }

        public bool IsRedemptionComplete
        {
            get { return _isRedemptionComplete; }
            set { SetProperty(ref _isRedemptionComplete, value); }
        }

        public IMvxCommand GoRedeem => new MvxCommand(ProcessRedemption);

        private async void ProcessRedemption()
        {
            if (!IsPinValid)
            {
                return;
            }

            Alerts.StartLoadingProgress();
            try
            {
                var coupon = await DataService.RedeemOffer(PinCode, StoreOffer);
                if (coupon != null)
                {
                    Coupon = coupon;
                    IsRedemptionComplete = true;
                }
                else
                {
                    Alerts.ShowToast("Error occured when redeeming the coupon");
                    return;
                }
            }
            finally
            {
                Alerts.StopLoadingProgress();
            }
        }

        public string PinCode
        {
            get => _pinCode;
            set
            {
                SetProperty(ref _pinCode, value);
                RaisePropertyChanged(() => IsPinValid);
            }
        }

        public bool IsPinValid => _pinCode?.Length == 4 && _pinCode.All(char.IsDigit);

        public IMvxCommand GoClose => new MvxCommand(() => NavigationService.Close(this));
        public IMvxCommand GoRedemptionFinish => new MvxCommand(() => NavigationService.Close(this));
        public IMvxCommand GoBarcodeMode => new MvxCommand(() =>
            {
                Store.ShowQrCodeByDefault = !Store.ShowQrCodeByDefault;
                RaisePropertyChanged(() => Store);
            });

        public override async void Prepare(StoreDetailsRequest request)
        {
            StoreOffer = request.Offer;
            Store = request.Store;

            StoreOffer.StoreId = (int)Store.Id;

            RaisePropertyChanged(() => IsPinValid);

            Task.Run(async () =>
            {
                Coupon = await DataService.GetRedeemedCoupon(StoreOffer.Id);
                if (!string.IsNullOrEmpty(Coupon?.Code))
                {
                    IsRedemptionComplete = true;
                }

//                // todo: remove/clean this part. it's introduced as code generator falls down if empty value
//                Coupon = Coupon ?? new RedeemedCoupon
//                {
//                    Code = Guid.NewGuid().ToString("N")
//                };

                IsBusy = false;
            });
        }

        public RedeemedCoupon Coupon
        {
            get => _coupon;
            set
            {
                SetProperty(ref _coupon, value);
                CouponCode = _coupon?.Code;
            }
        }

        public string CouponCode
        {
            get => _couponCode;
/*
            get
            {
                var v = "11112222";//string.IsNullOrWhiteSpace(_couponCode) ? "11112222" : _couponCode.ToUpper();
                Console.WriteLine("====================    >>>>>    >>>>>" + v);
                return v;
            }
*/
            set
            {
                SetProperty(ref _couponCode, value);
                CouponCodeFormatted = _coupon?.Code;
            }
        }

        public string CouponCodeFormatted
        {
            get => _couponCodeFormatted;
            set
            {
                var code = "";
                var parts = new List<string>();
                var partsCount = (value?.Length / 4) + 1;
                for (int i = 0; i < partsCount; i++)
                {
                    var count = (i + 1) * 4 - value.Length;
                    count = count <= 0 ? 4 : value.Length - i * 4;
                    parts.Add(value.Substring(i * 4, count));
                }

                code = string.Join(" ", parts);
                SetProperty(ref _couponCodeFormatted, code);
            }
        }
    }
}