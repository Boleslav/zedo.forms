﻿namespace Zedo.Core.ViewModels.Portal
{
    public class HomeItemsListViewModelRequest
    {
        public HomeItemsListViewModelRequest(StoreCategoryTypes storeCategoryType)
        {
            StoreCategoryType = storeCategoryType;
        }

        public StoreCategoryTypes StoreCategoryType { get; set; }
    }
}