﻿namespace Zedo.Core.ViewModels.Portal
{
    public class StoreDetailsViewModelRequest
    {
        public long StoreId { get; set; }
        public string StoreTitle { get; set; }
        public StoreDetailsViewModelRequest(long storeId, string storeTitle)
        {
            StoreId = storeId;
            StoreTitle = storeTitle;
        }
    }
}