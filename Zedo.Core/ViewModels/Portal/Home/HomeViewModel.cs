using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using Zedo.Core.ViewModels.Api;

namespace Zedo.Core.ViewModels.Portal
{
    public class HomeViewModel : BaseViewModel
    {
        private AdItem _adBanner;
        public IMvxCommand GoMalls => new MvxCommand(NavigateMalls);
        public IMvxCommand GoProductTypes => new MvxCommand(NavigateProductTypes);
        public IMvxCommand GoLocations => new MvxCommand(NavigateLocation);
        public IMvxCommand GoBrands => new MvxCommand(NavigateBrands);

        public IMvxCommand GoOffers => new MvxCommand(NavigateOffers);
        public IMvxCommand GoOffersNew => new MvxCommand(NavigateOffersNew);

        private void NavigateProductTypes()
        {
            var request = new HomeItemsListViewModelRequest(StoreCategoryTypes.ProductType);
            NavigationService.Navigate<HomeItemsListViewModel, HomeItemsListViewModelRequest>(request);
        }

        private void NavigateLocation()
        {
            var request = new HomeItemsListViewModelRequest(StoreCategoryTypes.Location);
            NavigationService.Navigate<HomeItemsListViewModel, HomeItemsListViewModelRequest>(request);
        }

        private void NavigateBrands()
        {
            var request = new HomeItemsListViewModelRequest(StoreCategoryTypes.Brand);
            NavigationService.Navigate<HomeItemsListViewModel, HomeItemsListViewModelRequest>(request);
        }

        private void NavigateMalls()
        {
            var request = new HomeItemsListViewModelRequest(StoreCategoryTypes.Mall);
            NavigationService.Navigate<HomeItemsListViewModel, HomeItemsListViewModelRequest>(request);
        }
        private void NavigateOffers()
        {
            var request = new StoresListViewModelRequest()
            {
                RequestingInstanceType = StoreCategoryTypes.AllOffers
            };
            NavigationService.Navigate<StoresListViewModel, StoresListViewModelRequest>(request);
        }
        private void NavigateOffersNew()
        {
            var request = new StoresListViewModelRequest()
            {
                RequestingInstanceType = StoreCategoryTypes.NewOffers
            };
            NavigationService.Navigate<StoresListViewModel, StoresListViewModelRequest>(request);
        }

        public override async void Prepare()
        {
            base.Prepare();

            AdBanner = await DataService.GetAd(MobilePages.Home);
             
//            DataService.ActivateUser(10, "R-0234");
        }

        public AdItem AdBanner
        {
            get => _adBanner;
            set => SetProperty(ref _adBanner, value);
        }

    }
}