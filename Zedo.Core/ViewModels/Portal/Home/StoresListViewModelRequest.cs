﻿namespace Zedo.Core.ViewModels.Portal
{
    public class StoresListViewModelRequest
    {
        public StoreCategoryTypes RequestingInstanceType { get; set; }
        public long RequestingInstanceId { get; set; }
        public string RequestingInstanceTitle { get; set; }
    }
}