﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using MvvmCross;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using JetBrains.Annotations;
using LawPilot.Mobile.Core.Services;

namespace Zedo.Core.ViewModels.Portal
{
    public abstract class BaseViewModel<T> : BaseViewModel, IMvxViewModel<T>
    {
        public abstract void Prepare(T parameter);
    }

    public abstract class BaseViewModel : MvxViewModel
    {
        private IDataService _dataService;
        private IIdentityService _identityService;
        private IAlertsService _alertsService;
        private bool _isBusy;

        protected IDataService DataService => _dataService = _dataService ?? Mvx.Resolve<IDataService>();
        protected IAlertsService Alerts => _alertsService = _alertsService ?? Mvx.Resolve<IAlertsService>();
        protected IIdentityService IdentityService => _identityService = _identityService ?? Mvx.Resolve<IIdentityService>();

        protected async Task<bool> EnsureSigningIn()
        {
            if (!IdentityService.IsSignedIn)
            {
                var presentation = new MvxBundle(new Dictionary<string, string>() { { "NavigationMode", "Modal" } });
                var request = new LoginSectionRequest { IsModal = true };
                await NavigationService.Navigate<LoginViewModel, LoginSectionRequest>(request, presentation);
            }

            return IdentityService.IsSignedIn;
        }

        [NotifyPropertyChangedInvocator]
        protected override bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
        {
            return base.SetProperty(ref storage, value, propertyName);
        }

        public bool IsBusy
        {
            get => _isBusy;
            set => SetProperty(ref _isBusy, value);
        }
    }
}