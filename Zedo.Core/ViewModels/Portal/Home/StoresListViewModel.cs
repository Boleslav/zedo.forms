﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace Zedo.Core.ViewModels.Portal
{
    public class StoresListViewModel : BaseViewModel<StoresListViewModelRequest>
    {
        private readonly IDataService _dataService;
        private string _search;
        private string _note;
        private IEnumerable<StoresListItemModel> _items;
        private FilterOptions _filter;
        private string _countTitle;
        private string _title;
        private bool _isLoading;
        private bool _isFilterVisible;
        private bool _isOffersFilteringAvailable;
        private bool _isNewOffersFilterActive;
        private FilterOptions _initialFilter;
        private bool _initialIsNewOffersFilterActive;

        public bool IsLoading
        {
            get { return _isLoading; }
            set { SetProperty(ref _isLoading, value); }
        }

        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        public string Note
        {
            get { return _note; }
            set { SetProperty(ref _note, value); }
        }

        public IEnumerable<StoresListItemModel> ItemsCache { get; set; }

        public IEnumerable<StoresListItemModel> Items
        {
            get { return _items; }
            set { SetProperty(ref _items, value); }
        }

        public IMvxCommand GoSelectItem => new MvxCommand<StoresListItemModel>(NavigateStore);
        public IMvxCommand GoFilter => new MvxCommand(NavigateFilter);
        public IMvxCommand GoClearFilter => new MvxCommand(ClearFilter);

        public IMvxCommand GoApplyFilter => new MvxCommand(ProcessApplyFilter);

        public bool IsFilterVisible
        {
            get => _isFilterVisible;
            set => SetProperty(ref _isFilterVisible, value);
        }

        private void NavigateFilter()
        {
            IsNewOffersFilterActive = _initialIsNewOffersFilterActive;

            IsFilterVisible = true;
            Filter = Mapper.Map<FilterOptions>(_initialFilter);
        }

        private void ClearFilter()
        {
            Filter.GoClear.Execute();
            IsNewOffersFilterActive 
                = OpenerRequest.RequestingInstanceType == StoreCategoryTypes.NewOffers;
        }

        public bool IsNewOffersFilterActive
        {
            get => _isNewOffersFilterActive;
            set => SetProperty(ref _isNewOffersFilterActive, value);
        }

        private async void ProcessApplyFilter()
        {
            if (Filter.Groups?.Any() == true)
            {
                if (Filter.Groups.Any(x => x.Items.All(y => !y.IsActive)))
                {
                    Alerts.ShowToast("At least one item per category must be selected.");
                    return;
                }
            }

            _initialIsNewOffersFilterActive = IsNewOffersFilterActive;

            _initialFilter = Filter;

            IsFilterVisible = false;

            IsLoading = true;
            await LoadData();
            IsLoading = false;
        }

        public FilterOptions Filter
        {
            get => _filter;
            set => SetProperty(ref _filter, value);
        }

        public StoresListViewModel(IDataService dataService)
        {
            _dataService = dataService;
        }

        private async void NavigateStore(StoresListItemModel item)
        {
            if (IsOffers)
            {
//                store = await _dataService.GetStoresByCategoryAsync();
//                FirstOrDefault(x => x.Id == store.Id);
            }
            var request = new StoreDetailsViewModelRequest(item.StoreId, item.StoreTitle);
            await NavigationService.Navigate<StoreDetailsViewModel, StoreDetailsViewModelRequest>(request);
        }

        private bool IsOffers => OpenerRequest?.RequestingInstanceType == StoreCategoryTypes.NewOffers 
                                 || OpenerRequest?.RequestingInstanceType == StoreCategoryTypes.AllOffers;

        public override async void Prepare(StoresListViewModelRequest request)
        {
            OpenerRequest = request;

            CountTitle = GetCountTitleByCategoryType().ToUpper();
            Title = GetTitleByCategoryType();

            IsLoading = true;

            IsOffersFilteringAvailable = IsOffers;

            Filter = _initialFilter = new FilterOptions
            {
                Groups = await GetFilterOptions(request)
            };

            await LoadData();
            IsLoading = false;
        }

        public bool IsOffersFilteringAvailable
        {
            get => _isOffersFilteringAvailable;
            set => SetProperty(ref _isOffersFilteringAvailable, value);
        }

        private async Task LoadData()
        {
            if (IsOffers)
            {
                ItemsCache = (await Mvx.Resolve<IDataService>().GetOffersAsync(IsNewOffersFilterActive))
                    .Select(x => new StoresListItemModel(x.StoreId, x.Title, x.StoreImage)
                        {
                            StoreTitle = "", // todo: ??
                            Image = x.StoreImage,
                            Title2 = x.PromoTypeTitle,
                            OfferImage = x.Image,
                            OfferId = x.Id,
                            IsOfferMode = true
                        }).ToArray();
            }
            else
            {
                var instanceIds = Filter.Groups?.FirstOrDefault()?.Items.Where(x => x.IsActive).Select(x => (long)x.Id).ToArray();

                ItemsCache = (await DataService.GetStoresByCategoryAsync(
                    instanceIds,
                    OpenerRequest.RequestingInstanceType)).Select(x => new StoresListItemModel(x.Id, x.Title, x.Image)
                {
                    StoreTitle = x.Title,
                    OffersCount = x.OffersCount,
                    HasOffers = x.HasOffers
                }).ToArray();
            }

            // for smooth refreshing on Android (iOS is fine). refreshing gets stuck when stores images can't be downloaded
            if (Device.RuntimePlatform == Device.Android)
            {
                // not on the first load as data already empty
                if (Items != null)
                {
                    Items = null;
                    await Task.Delay(75);
                }
            }

            FilterItems();
        }

        private async Task<IEnumerable<FilterOptionGroup>> GetFilterOptions(StoresListViewModelRequest request)
        {
            var currentCategoryId = request.RequestingInstanceId;
            bool isItemActive(long id) => currentCategoryId == 0 || currentCategoryId == id;

            var result = new List<FilterOptionGroup>();

            List<FilterOptionItem> items;
            switch (request.RequestingInstanceType)
            {
                case StoreCategoryTypes.Mall:
                    items = (await _dataService.GetMallsAsync()).Select(x => new FilterOptionItem(isItemActive(x.Id)) { Id = x.Id, Title = x.Title }).ToList();
                    result.Add(new FilterOptionGroup
                    {
                        Title = "MALLS",
                        Items = items
                    });
                    break;
                case StoreCategoryTypes.Location:
                    items = (await _dataService.GetStoreLocationsAsync()).Select(x => new FilterOptionItem(isItemActive(x.Id)) { Id = x.Id, Title = x.Title }).ToList();
                    result.Add(new FilterOptionGroup
                    {
                        Title = "LOCATIONS",
                        Items = items
                    });
                    break;
                case StoreCategoryTypes.Brand:
                    items = (await _dataService.GetBrandsAsync()).Select(x => new FilterOptionItem(isItemActive(x.Id)) { Id = x.Id, Title = x.Title }).ToList();
                    result.Add(new FilterOptionGroup
                    {
                        Title = "BRANDS",
                        Items = items
                    });
                    break;
                case StoreCategoryTypes.ProductType:
                    items = (await _dataService.GetProductTypesAsync()).Select(x => new FilterOptionItem(isItemActive(x.Id)) { Id = x.Id, Title = x.Title }).ToList();
                    result.Add(new FilterOptionGroup
                    {
                        Title = "CATEGORIES",
                        Items = items
                    });
                    break;
            }
            
            return result;
        }

        private string GetCountTitleByCategoryType()
        {
            return IsOffers ? "Offers" : "Stores";
        }

        public StoresListViewModelRequest OpenerRequest { get; set; }

        private string GetTitleByCategoryType()
        {
            if (OpenerRequest.RequestingInstanceId > 0)
            {
                return OpenerRequest.RequestingInstanceTitle;
            }

            switch (OpenerRequest.RequestingInstanceType)
            {
                case StoreCategoryTypes.NewOffers:
                    return "New Offers";
                case StoreCategoryTypes.AllOffers:
                    return "All Offers";
                case StoreCategoryTypes.Mall:
                    return "All Malls";
                case StoreCategoryTypes.Brand:
                    return "All Brands";
                case StoreCategoryTypes.ProductType:
                    return "All Products";
                case StoreCategoryTypes.Location:
                    return "All Locations";
            }

            return "";
        }

        public string CountTitle
        {
            get { return _countTitle; }
            set { SetProperty(ref _countTitle, value); }
        }

        private void FilterItems()
        {
            Items = ItemsCache.Where(x => string.IsNullOrEmpty(Search) || x.Title.ToLower().Contains(Search.ToLower()))
                .ToArray();
            Note = $"{Items.Count()}" + ((ItemsCache.Any() ? $" OF {ItemsCache.Count()}" : "")) + $" {CountTitle}";
        }

        public string Search
        {
            get { return _search; }
            set
            {
                SetProperty(ref _search, value);
                FilterItems();
            }
        }
    }

    public class StoresListItemModel
    {
        public long StoreId { get; set; }
        public string StoreTitle { get; set; }
        public string Title { get; set; }
        public string Title2 { get; set; }
        public string Image { get; set; }
        public string OfferImage { get; set; }
        public long OfferId { get; set; }
        public int OffersCount { get; set; }
        public bool HasOffers { get; set; }
        public bool IsOfferMode { get; set; }

        public StoresListItemModel(long storeId, string title, string image)
        {
            StoreId = storeId;
            Title = title;
            Image = image;
        }
    }
}