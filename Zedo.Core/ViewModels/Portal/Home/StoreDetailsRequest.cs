﻿namespace Zedo.Core.ViewModels.Portal
{
    public class StoreDetailsRequest
    {
        public StoreItem Store { get; set; }
        public StoreOffer Offer { get; set; }
    }
}