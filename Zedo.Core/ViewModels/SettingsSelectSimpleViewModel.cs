using System.Collections.Generic;
using System.Linq;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;


namespace Zedo.Core.ViewModels
{
    public class SettingsSelectSimpleViewModel : MvxViewModel<SelectSimpleRequest, SelectSimpleItem>
    {
        private readonly IMvxNavigationService _navigationService;

        private string _header;
        private IEnumerable<SelectSimpleItem> _items;
        private IEnumerable<SelectSimpleItem> _itemsCach;
        private string _title;
        private string _search;
        public IMvxCommand GoSelectItem => new MvxCommand<SelectSimpleItem>(ProcessItemSelected);

        public bool IsSearchable { get; set; }

        public SettingsSelectSimpleViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        private async void ProcessItemSelected(SelectSimpleItem item)
        {
            await _navigationService.Close(this, item);
        }

        public override void Prepare(SelectSimpleRequest request)
        {
            _itemsCach = Items = request.Items;
            Header = request.Header;
            Title = request.Title;
            IsSearchable = request.IsSearchable;
        }

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        public string Header
        {
            get => _header;
            set => SetProperty(ref _header, value);
        }

        public IEnumerable<SelectSimpleItem> Items
        {
            get => _items;
            set => SetProperty(ref _items, value);
        }

        public string Search
        {
            get { return _search; }
            set
            {
                SetProperty(ref _search, value);
                if (IsSearchable)
                {
                    ApplyFilter();
                }
            }
        }

        private void ApplyFilter()
        {
            var search = Search?.ToLower();
            Items = _itemsCach?.Where(x => x.Title.ToLower().Contains(search)).ToArray();
        }
    }
}