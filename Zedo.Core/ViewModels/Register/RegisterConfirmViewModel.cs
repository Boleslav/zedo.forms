


using AutoMapper;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using Zedo.Core.Data.Repository;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.ViewModels
{
    public class RegisterConfirmViewModel : BaseViewModel
    {
        private string _phone;
        public IMvxCommand GoActivation => new MvxCommand(ProcessActivation);

        UserSettings _currentRegistration;

        private async void ProcessActivation()
        {
            Alerts.StartLoadingProgress("");
            try
            {
                var user = await DataService.RegisterUser(_currentRegistration);

                if (user == null)
                {
                    Alerts.ShowToast("Error occured when registering");
                    return;
                }

                _currentRegistration.CustomerId = user.CustomerId;
                _currentRegistration.IsActivationRequested = true;
                _currentRegistration.ActivationCode = user.ActivationCode;

                IdentityService.UpdateSignIn(_currentRegistration);
            }
            finally
            {
                Alerts.StopLoadingProgress();
            }

            await NavigationService.Navigate<RegisterActivationViewModel>();
        }

        public string Phone
        {
            get => _phone;
            set => SetProperty(ref _phone, value);
        }

        public RegisterConfirmViewModel()
        {

        }

        public override void Prepare()
        {
            base.Prepare();

            _currentRegistration = IdentityService.CurrentIdentity();
            if (_currentRegistration == null)
            {
                NavigationService.Close(this);
                return;
            }

            Phone = "+974 " + _currentRegistration.Phone;
        }
    }
}