


using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.Plugin.Messenger;
using MvvmCross.Presenters.Hints;
using MvvmCross.ViewModels;
using Zedo.Core.Messages;
using Zedo.Core.ViewModels.Local;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.ViewModels
{
    public class RegisterActivationViewModel : BaseViewModel
    {
        private readonly IMvxMessenger _messenger;
        private string _code;

        public IMvxCommand GoActivation => new MvxCommand(ProcessActivation);
        public IMvxCommand GoResend => new MvxCommand(ProcessResend);
        public IMvxCommand GoRestart => new MvxCommand(ProcessRestart);

        public RegisterActivationViewModel(IMvxMessenger messenger)
        {
            _messenger = messenger;
        }

        public string Code
        {
            get => _code;
            set => SetProperty(ref _code, value);
        }

        private async void ProcessActivation()
        {
            Alerts.StartLoadingProgress();

            try
            {
                var currentRegistration = IdentityService.CurrentIdentity();
                var isSuccess = await DataService.ActivateUser(currentRegistration.CustomerId, Code);

                if (!isSuccess)
                {
                    Alerts.ShowToast("Error occured when activating");
                    return;
                }

                currentRegistration.IsActivated = true;
                IdentityService.UpdateSignIn(currentRegistration);
            }
            finally
            {
                Alerts.StopLoadingProgress();
            }

            var isModal = Mvx.Resolve<ILoginSectionService>().IsLoginSectionModal;
            if (isModal)
            {
                await NavigationService.ChangePresentation(new MvxClosePresentationHint(this));
            }
            else
            {
                await NavigationService.Navigate<HomeRootViewModel>();
            }
        }

        private async void ProcessResend()
        {
            Alerts.StartLoadingProgress();

            try
            {
                var currentRegistration = IdentityService.CurrentIdentity();
                var user = await DataService.ResendUserActivationCode(currentRegistration.CustomerId);

                if (user != null)
                {
                    currentRegistration.PhoneCode = user.PhoneCode;
                    currentRegistration.ActivationCode = user.ActivationCode;
                    IdentityService.UpdateSignIn(currentRegistration);

                    Alerts.ShowToast("New code sent successfully");
                }
                else
                {
                    Alerts.ShowToast("Error occured when sending new code");
                }
            }
            finally
            {
                Alerts.StopLoadingProgress();
            }
        }

        private async void ProcessRestart()
        {
            IdentityService.LogOut();
            var hint = new MvxPopToRootPresentationHint();
            await NavigationService.ChangePresentation(hint);
            await NavigationService.Navigate<LoginViewModel>();

//            await NavigationService.Close(this);

//            await NavigationService.Navigate<LoginViewModel>();
       }
    }
}