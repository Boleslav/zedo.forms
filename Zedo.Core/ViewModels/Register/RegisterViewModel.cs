using System;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using Zedo.Core.Data.Repository;
using Zedo.Core.ViewModels.Portal;


namespace Zedo.Core.ViewModels
{
    public class RegisterViewModel : BaseViewModel
    {
        private SelectSimpleItem<Country> _nationalitySelected;
        private SelectSimpleItem _genderSelected;

        public IMvxCommand GoNationality => new MvxCommand(NavigateNationality);
        public IMvxCommand GoGender => new MvxCommand(NavigateGender);
        public IMvxCommand GoTerms => new MvxCommand(NavigateTerms);

        SelectSimpleItem<Country> _nationalityDefaultValue = new SelectSimpleItem<Country>(0, "<select your nationality>", true, new Country(0, ""));
        SelectSimpleItem _genderDefaultValue = new SelectSimpleItem(0, "<select your gender>");
        private string _name;
        private string _phone;
        private string _email;
        private string _password;
        private string _password2;
        private SelectSimpleItem<Country>[] _countriesCache;

        public RegisterViewModel()
        {
            _nationalitySelected = _nationalityDefaultValue;
            _genderSelected = _genderDefaultValue;
        }

        public override async Task Initialize()
        {
            await base.Initialize();

            var countries = await DataService.GetCountriesAvailable();
            _countriesCache = countries.Select(x => new SelectSimpleItem<Country>(x.Id, x.Title, false, x)).ToArray();
        }

        private async void NavigateTerms()
        {
            if (string.IsNullOrEmpty(Name)
                || string.IsNullOrEmpty(Email)
                || string.IsNullOrEmpty(Phone)
                || NationalitySelected.Id == 0
                || NationalitySelected.Id == 0
                || string.IsNullOrEmpty(Password)
                || Password != Password2)
            {
                // todo: error notification
                return;
            }

            var user = new UserSettings()
            {
                CustomerName = Name,
                Email = Email,
                Phone = Phone,
                Gender = GenderSelected.Id == 0 ? null : (byte?)GenderSelected.Id,
                CountryId = (int)NationalitySelected.Value.Id,
                Password = Password
            };

            IdentityService.SignIn(user);
            await NavigationService.Navigate<RegisterTermsViewModel>();
        }

        private async void NavigateNationality()
        {
            var model = new SelectSimpleRequest()
            {
                Header = "Nationality",
                Title = "SELECT YOUR NATIONALITY",
                Items = _countriesCache,
                IsSearchable = true
            };

            var result = await NavigationService.Navigate<SettingsSelectSimpleViewModel, SelectSimpleRequest, SelectSimpleItem>(model);
            if (result != null)
            {
                NationalitySelected = (SelectSimpleItem<Country>)result;
            }
        }

        private async void NavigateGender()
        {
            var model = new SelectSimpleRequest()
            {
                Header = "Gender",
                Title = "SELECT YOUR GENDER",
                Items = DataService.GetGendersAvailable()
            };

            var result = await NavigationService.Navigate<SettingsSelectSimpleViewModel, SelectSimpleRequest, SelectSimpleItem>(model);
            if (result != null)
            {
                GenderSelected = result;
            }
        }

        public SelectSimpleItem<Country> NationalitySelected
        {
            get => _nationalitySelected;
            set => SetProperty(ref _nationalitySelected, value);
        }

        public SelectSimpleItem GenderSelected
        {
            get => _genderSelected;
            set => SetProperty(ref _genderSelected, value);
        }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public string Phone
        {
            get => _phone;
            set => SetProperty(ref _phone, value);
        }

        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        public string Password2
        {
            get => _password;
            set => SetProperty(ref _password2, value);
        }
    }
}