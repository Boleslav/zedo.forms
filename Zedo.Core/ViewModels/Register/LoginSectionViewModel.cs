



using System.Collections.Generic;
using System.Threading.Tasks;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using Zedo.Core.ViewModels.Local;

namespace Zedo.Core.ViewModels.Portal
{
    public class LoginSectionViewModel : MvxViewModel<LoginSectionRequest>
    {
        public LoginSectionViewModel()
        {
            ShowInitialViewModelsCommand = new MvxAsyncCommand(ShowInitialViewModels);
            //            ShowTabsRootBCommand = new MvxAsyncCommand(async () => await NavigationService.Navigate<TabsRootBViewModel>());
        }

        public IMvxAsyncCommand ShowInitialViewModelsCommand { get; private set; }

        //        public IMvxAsyncCommand ShowTabsRootBCommand { get; private set; }

        private async Task ShowInitialViewModels()
        {
            var tasks = new List<Task>();
            tasks.Add(NavigationService.Navigate<LoginViewModel>());
            await Task.WhenAll(tasks);
        }

        public override void Prepare(LoginSectionRequest parameter)
        {
            Mvx.Resolve<ILoginSectionService>().IsLoginSectionModal = parameter?.IsModal == true;
        }
    }
}