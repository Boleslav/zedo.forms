


using System.Threading.Tasks;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using Zedo.Core.Data.Repository;
using Zedo.Core.ViewModels.Api;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.ViewModels
{
    public class RegisterTermsViewModel : BaseViewModel
    {
        private bool _isNewsSubscriber;
        private bool _isTermsSelected;
        private IMvxNavigationService _navigationService;
        private UserSettings _userSettings;

        public IMvxCommand GoConfirm => new MvxCommand(NavigateConfirm);

        public IMvxCommand GoTerms => new MvxCommand(() => IsTermsSelected = !IsTermsSelected);
        public IMvxCommand GoOffers => new MvxCommand(() => IsNewsSubscriber = !IsNewsSubscriber);

        public RegisterTermsViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public bool IsTermsSelected
        {
            get { return _isTermsSelected; }
            set { SetProperty(ref _isTermsSelected, value); }
        }

        public override Task Initialize()
        {
            IsNewsSubscriber = IdentityService.CurrentIdentity().IsNewsSubscriber;

            return base.Initialize();
        }

        public bool IsNewsSubscriber
        {
            get { return _isNewsSubscriber; }
            set { SetProperty(ref _isNewsSubscriber, value); }
        }

        private async void NavigateConfirm()
        {
            if (!IsTermsSelected)
            {
                // todo error notification
                return;
            }

            var currentRegistration = IdentityService.CurrentIdentity();
            currentRegistration.IsNewsSubscriber = IsNewsSubscriber;
            IdentityService.UpdateSignIn(currentRegistration);

            await _navigationService.Navigate<RegisterConfirmViewModel>();
        }
    }
}