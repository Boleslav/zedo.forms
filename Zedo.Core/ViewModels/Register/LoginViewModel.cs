using System;
using System.Threading.Tasks;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.Plugin.Messenger;
using Zedo.Core.Messages;
using Zedo.Core.ViewModels.Local;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.ViewModels
{
    public class LoginViewModel : BaseViewModel<LoginSectionRequest>
    {
        private readonly IMvxNavigationService _navigationService;
        private readonly IMvxMessenger _messenger;
        private string _email;
        private string _password;

        public bool IsModal { get; set; }

        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        public LoginViewModel(IMvxNavigationService navigationService, IMvxMessenger messenger)
        {
            _navigationService = navigationService;
            _messenger = messenger;

            IsModal = Mvx.Resolve<ILoginSectionService>().IsLoginSectionModal;
        }
        
        public override Task Initialize()
        {
            return base.Initialize();
        }

        public IMvxCommand GoLogin => new MvxCommand(async() => await ProcessLogin());
        public IMvxCommand GoRegister => new MvxCommand(NavigateRegister);
        public IMvxCommand GoForgotPassword => new MvxCommand(NavigateForgotPassword);
        public IMvxCommand GoGuest => new MvxCommand(async() => await NavigateHome());
        public IMvxCommand GoCancel => new MvxCommand(async() => await ProcessCancel());

        private async Task ProcessCancel()
        {
            await NavigationService.Close(this);
        }

        private async Task ProcessLogin()
        {
            Alerts.StartLoadingProgress();

            try
            {
                var user = await DataService.Login(Email, Password);
                if (user != null)
                {
                    IdentityService.SignIn(user);
                }
                else
                {
                    Alerts.ShowToast("Email or password incorrect");
                    return;
                }
            }
            finally
            {
                Alerts.StopLoadingProgress();
            }

            if (IsModal)
            {
                await ProcessCancel();
            }
            else
            {
                await NavigateHome();
            }
        }

        private void NavigateForgotPassword()
        {
            _navigationService.Navigate<ForgotPasswordViewModel>();
        }

        private void NavigateRegister()
        {
            _navigationService.Navigate<RegisterViewModel>();
        }

        private async Task NavigateHome()
        {
            await _navigationService.Navigate<HomeRootViewModel>();

//            _messenger.Publish(new RootPageRequestMessage(this));
        }

        public override void Prepare(LoginSectionRequest parameter)
        {
            IsModal = parameter?.IsModal == true;
            Mvx.Resolve<ILoginSectionService>().IsLoginSectionModal = parameter?.IsModal == true;
        }

    }
}