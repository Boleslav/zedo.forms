﻿namespace Zedo.Core.Data.Repository
{
    public interface ILiteDbService
    {
        LiteDBRepository<T> GetRepository<T>() where T : class, IDbLiteEntity;
    }
}