﻿namespace Zedo.Core.Data.Repository
{
    public interface IDbLiteEntity
    {
        int Id { get; set; }
    }
}