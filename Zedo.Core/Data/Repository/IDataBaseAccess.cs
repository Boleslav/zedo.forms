﻿namespace Zedo.Core.Data.Repository
{
    public interface IDataBaseAccess
    {
        string DatabasePath();
    }
}