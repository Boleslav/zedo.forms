﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using LiteDB;
using Xamarin.Forms;

namespace Zedo.Core.Data.Repository
{
    public class LiteDBRepository<T> where T : class, IDbLiteEntity
    {
        private readonly bool _isValueSingleton;
        protected LiteCollection<T> _collection;

        public LiteDBRepository(bool isValueSingleton = false)
        {
            _isValueSingleton = isValueSingleton;
            var db = new LiteDatabase(DependencyService.Get<IDataBaseAccess>().DatabasePath());
            _collection = db.GetCollection<T>();

            var mapper = BsonMapper.Global;

            mapper.Entity<T>().Id(x => x.Id);
        }

        public virtual T UpSertItem(T item)
        {
            if (_isValueSingleton)
            {
                item.Id = 1;
                _collection.Upsert(item);
            }
            else if (item.Id == 0)
            {
                item.Id = _collection.Max() + 1;
                _collection.Insert(item);
            }
            else
            {
                _collection.Update(item);
            }

            return item;
        }

        public virtual T DeleteItem(T item)
        {
            var c = _collection.Delete(i => i.Id == item.Id);
            return c == 0 ? (T)null : item;
        }

        public virtual void DeleteAll()
        {
            var c = _collection.Delete(i => true);
        }

        public virtual IEnumerable<T> ReadAllItems()
        {
            var all = _collection.FindAll();
            return new List<T>(all);
        }
    }
}