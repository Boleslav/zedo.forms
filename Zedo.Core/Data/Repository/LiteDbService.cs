﻿using System;
using System.Collections.Generic;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.Data.Repository
{
    public class LiteDbService : ILiteDbService
    {
        private Dictionary<Type, object> repos;
        public LiteDbService()
        {
            repos = new Dictionary<Type, object>
            {
                {
                    typeof(UserSettings), new LiteDBRepository<UserSettings>(true)
                },
                {
                    typeof(NotificationItem), new LiteDBRepository<NotificationItem>()
                }
//                new LiteDBRepository<User>(),
            };
        }

        public LiteDBRepository<T> GetRepository<T>() where T : class, IDbLiteEntity
        {
            return repos[typeof(T)] as LiteDBRepository<T>;
        }
    }
}