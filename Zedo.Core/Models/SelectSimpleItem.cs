namespace Zedo.Core.ViewModels
{
    public class SelectSimpleItem
    {
        public SelectSimpleItem(long id, string title, bool isActive = false)
        {
            Id = id;
            Title = title;
            IsActive = isActive;
        }

        public long Id { get; set; }
        public string Title { get; set; }
        public bool IsActive { get; set; }
    }

    public class SelectSimpleItem<T> : SelectSimpleItem where T : class
    {
        public SelectSimpleItem(long id, string title, bool isActive = false, T value = null) : base(id, title, isActive)
        {
            Value = value;
        }

        public T Value { get; set; }
    }
}