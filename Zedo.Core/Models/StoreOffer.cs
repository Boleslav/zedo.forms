﻿using System;

namespace Zedo.Core.ViewModels.Portal
{
    public class StoreOffer : StoreCategoryItem
    {
        private bool _isHot;
        private int _daysLeft;
        private DateTime? _to;

        public StoreOffer(long id, string title, string image) : base(id, title, image)
        {
        }

        public int DaysLeft
        {
            get => _daysLeft;
        }

        public string DaysLeftFormatted { get; set; }
        public bool IsRedeemed { get; set; }
        public bool IsYearly { get; set; }

        public DateTime? From { get; set; }
        public DateTime? To
        {
            get => _to;
            set
            {
                _to = value;
                var now = DateTime.UtcNow;
                if (now < From)
                {
                    _daysLeft = 0;
                }
                else if(_to == null || _to < now)
                {
                    _daysLeft = 0;
                }
                else
                {
                    _daysLeft = (int) ((_to.Value - now).TotalDays + .5);
                }
            }
        }

        public string StoreImage { get; set; }
        public string ProductName { get; set; }
        public int StoreId { get; set; }

        public bool IsExpired { get; set; }
        public string PromoTypeTitle { get; set; }


        public bool IsHot
        {
            get => _isHot || DaysLeft < 15;
            set => _isHot = value;
        }

        public bool IsVeryHot => DaysLeft < 15 && DaysLeft > 0;
    }
}