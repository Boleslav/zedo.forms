﻿namespace Zedo.Core.ViewModels.Api
{
    public class MallModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }

    }
}