﻿using System;

namespace Zedo.Core.ViewModels.Portal
{
    public class RedeemedCoupon
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public DateTime RedeemedAt { get; set; }
        public long OfferId { get; set; }
        public long UserId { get; set; }
    }
}