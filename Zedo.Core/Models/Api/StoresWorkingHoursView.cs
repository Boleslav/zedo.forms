﻿namespace Zedo.Core.ViewModels.Api
{
    public class StoresWorkingHoursView
    {
        public int StoreWorkTimeID { get; set; }
        public int FkStoreId { get; set; }
        public string WorkingHours { get; set; }
    }
}