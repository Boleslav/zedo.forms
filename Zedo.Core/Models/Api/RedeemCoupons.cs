﻿using System;

namespace Zedo.Core.Models.Api
{
    public partial class RedeemCoupons
    {
        public long RedeemID { get; set; }
        public string Ref { get; set; }
        public DateTime RedeemDate { get; set; }
        public string PinCustomer { get; set; }
        public string PinStore { get; set; }
        public long FkCustomerID { get; set; }
        public long FkStoreOfferID { get; set; }

//        public virtual Customers Customers { get; set; }
//        public virtual StoreCoupon StoreCoupon { get; set; }
    }
}