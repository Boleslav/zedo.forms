﻿using System.Collections.Generic;
using System.Linq;

namespace Zedo.Core.Models.Api
{
    public class GenericResult
    {
        public bool Status => MessagesList?.Any() != true;
        public List<string> MessagesList { get; set; }

        public GenericResult()
        {
            //            MessagesList = new List<string>(); 
        }

        public virtual bool IsSuccess { get; set; }
    }

    public class GenericResult<T> : GenericResult
    {
        public T Value { get; set; }

        public override bool IsSuccess => Value != null;
    }

}
