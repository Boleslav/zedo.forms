﻿using System.Collections.Generic;

namespace Zedo.Core.ViewModels.Api
{
    public class StoresView
    {
        public int StoreID { get; set; }
        public string StoreName { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Floor { get; set; }
        public string StoreLogo { get; set; }
        public string Website { get; set; }
        public int? LocationID { get; set; }
        public string LocationName { get; set; }
        public int? OffersCount { get; set; }
        public string OffersTitle { get; set; }
        public string GoogleMapUrl { get; set; }

        public IEnumerable<StoresProductsView> Products { get; set; }

        public IEnumerable<StoresWorkingHoursView> WorkingHours { get; set; }

        public IEnumerable<StoresOffersView> Offers { get; set; }

        public bool IsCustomerFavorite { get; set; }

        public IEnumerable<string> Gallery { get; set; }

    }
}