﻿namespace Zedo.Core.ViewModels.Api
{
    public class MallsView
    {
        public int MallID { get; set; }
        public string MallName { get; set; }
        public string MallLogo { get; set; }
    }
}