﻿using System;

namespace Zedo.Core.ViewModels.Api
{
    public class StoresOffersView
    {
        public long StoreCouponID { get; set; }
        public int StoreId { get; set; }
        public string ProductName { get; set; }
        public string DaysLeft { get; set; }
        public string CouponImage { get; set; }
        public string CouponTitle { get; set; }
        public string StoreLogo { get; set; }
        public bool IsYearly { get; set; }
        public DateTime? CouponFrom { get; set; }
        public DateTime? CouponTo { get; set; }

        // specific
        public bool IsRedeemed { get; set; }

    }
}