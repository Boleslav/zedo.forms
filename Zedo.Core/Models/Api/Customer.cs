﻿namespace Zedo.Core.ViewModels.Api
{
    public class Customer
    {
        public long CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Phone { get; set; }
        public string Pin { get; set; }
        public bool IsActive { get; set; }
        public bool IsRemoved { get; set; }
        public string ActivationCode { get; set; }
        public bool IsPhoneActive { get; set; }
        public string PhoneCode { get; set; }
        public byte? Gender { get; set; }
        public bool IsNewsSubscriber { get; set; }
        public int FKCountryID { get; set; }
    }
}