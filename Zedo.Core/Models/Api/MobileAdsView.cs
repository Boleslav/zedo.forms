﻿namespace Zedo.Core.ViewModels.Api
{
    public enum MobilePages : byte
    {
        Home = 1,
        Malls = 2,
        Stores = 3,
        Locations = 4,
        Brands = 5
    }

    public class MobileAdsView
    {
        public int AdsID { get; set; }
        public string AdLink { get; set; }
        public string AdImage { get; set; }
        public MobilePages MobilePageID { get; set; }
    }
}