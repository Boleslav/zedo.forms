﻿namespace Zedo.Core.ViewModels.Api
{
    public class StoresProductsView
    {
        public int StoreProductCategoryID { get; set; }
        public int FkStoreID { get; set; }
        public string ProductName { get; set; }
        public string ProductImage { get; set; }
    }
}