﻿namespace Zedo.Core.ViewModels.Api
{
    public class BrandsView
    {
        public int BrandID { get; set; }
        public string BrandName { get; set; }
        public string BrandLogo { get; set; }
    }
}