﻿namespace Zedo.Core.ViewModels.Api
{
    public class ProductCategoriesView
    {
        public int ProductCategoryID { get; set; }
        public string ProductCategoryName { get; set; }
        public string ProductCategoryImage { get; set; }
    }
}