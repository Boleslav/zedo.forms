﻿namespace Zedo.Core.ViewModels.Api
{
    public class Locations
    {
        public int LocationID { get; set; }
        public string NameAr { get; set; }
        public string NameEn { get; set; }
        public bool IsRemoved { get; set; }
    }
}