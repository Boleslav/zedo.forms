﻿namespace Zedo.Core.ViewModels.Api
{
    public class CountryView
    {
        public int CountryID { get; set; }
        public string CountryNameEn { get; set; }
        public string CountryNameAr { get; set; }
        public string CountryCode { get; set; }
    }
}