using System.Collections.Generic;

namespace Zedo.Core.ViewModels
{
    public class SelectSimpleRequest
    {
        public string Header { get; set; }
        public string Title { get; set; }
        public IEnumerable<SelectSimpleItem> Items { get; set; }
        public bool IsSearchable { get; set; }
    }
}