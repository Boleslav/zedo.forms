﻿namespace Zedo.Core.ViewModels.Portal
{
    public enum StoreCategoryTypes
    {
        General,
        Mall,
        Location,
        Brand,
        ProductType,
        NewOffers,
        AllOffers
    }
}