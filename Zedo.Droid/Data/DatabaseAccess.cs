﻿using System.IO;
using Android.Provider;
using Zedo.Core;
using Zedo.Core.Data.Repository;
using Zedo.Droid.Data;

[assembly: Xamarin.Forms.Dependency(typeof(DatabaseAccess))]
namespace Zedo.Droid.Data
{
    public class DatabaseAccess : IDataBaseAccess
    {
        public string DatabasePath()
        {
            var path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), CoreApp.OfflineDatabaseName);

            if (!File.Exists(path))
            {
                File.Create(path).Dispose();
            }

            return path;
        }
    }
}