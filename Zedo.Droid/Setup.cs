using CarouselView.FormsPlugin.Android;
using MvvmCross;
using MvvmCross.Forms.Platforms.Android.Core;
using MvvmCross.Forms.Presenters;
using MvvmCross.Plugin;
using MvvmCross.Presenters;
using MvvmCross.ViewModels;
using Xamarin.Forms;
using Zedo.Core;
using Zedo.Core.Forms;
using Zedo.iOS.Extensions.Presentation;
using ZXing.Net.Mobile.Forms.Android;

namespace Zedo.Droid
{
    public class Setup : MvxFormsAndroidSetup<CoreApp, App>
    {
        protected override void InitializeApp(IMvxPluginManager pluginManager, IMvxApplication app)
        {
            base.InitializeApp(pluginManager, app);
            CarouselViewRenderer.Init();
            Platform.Init();
        }

        protected override IMvxFormsPagePresenter CreateFormsPagePresenter(IMvxFormsViewPresenter viewPresenter)
        {
            var generalPresenter = new GeneralPresenter(viewPresenter);
            Mvx.RegisterType<IMvxFormsPagePresenter>(() => generalPresenter);
            return generalPresenter;
        }
    }
}
