﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Zedo.Android.Extensions.Renderers;
using Zedo.Core.Forms.Controls;
using Color = Android.Graphics.Color;
using View = Android.Views.View;

[assembly: ExportRenderer(typeof(AlertPanel), typeof(AlertPanelRenderer))]
namespace Zedo.Android.Extensions.Renderers
{
    public class AlertPanelRenderer : FrameRenderer
    {
        private AlertPanel control;
        private BottomSheetDialogEx dialog;

        public AlertPanelRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
        {
            base.OnElementChanged(e);

            // supporting the only active element through the UI at the moment
            control = e.NewElement as AlertPanel;

            if (control != null)
            {
                control.OnAppearing = StartAppearing;
                control.OnDisappearing = StartDisappearing;
            }
        }

        private void StartAppearing()
        {
            dialog = new BottomSheetDialogEx(Context);
            dialog.OnBackPressing = () =>
            {
                control.IsVisible = false;
                control?.OnDisappeared?.Invoke();
                return true;
            };

            this.RemoveFromParent();
            var ll = new LinearLayout(Context);
            ll.SetPadding(0, 0, 0, GetNavigationBarHeight());

            var llInner = new LinearLayout(Context);
            llInner.Orientation = Orientation.Vertical;
            ll.AddView(llInner);
            
            var lp = new LayoutParams(LayoutParams.MatchParent, LayoutParams.MatchParent);
           
            var lpInner = new LinearLayout.LayoutParams(LayoutParams.MatchParent, LayoutParams.MatchParent);
//            lpInner.BottomMargin = GetNavigationBarHeight();

            llInner.LayoutParameters = lpInner;
//            ll.SetBackgroundColor(global::Android.Graphics.Color.Blue);
            
            llInner.WeightSum = 100f;

            var stretcher = new LinearLayout(Context);
            stretcher.LayoutParameters = new LinearLayout.LayoutParams(LayoutParams.MatchParent, 0, 60f);
//            stretcher.SetBackgroundColor(Color.Green);

            llInner.AddView(stretcher);

            var cont = new LinearLayout(Context);
            cont.LayoutParameters = new LinearLayout.LayoutParams(LayoutParams.MatchParent, 0, 40f);

//            cont.SetBackgroundColor(Color.Red);
            llInner.AddView(cont);
            this.LayoutParameters = new LinearLayout.LayoutParams(LayoutParams.MatchParent, LayoutParams.MatchParent);
            cont.AddView(this);

            dialog.AddContentView(ll, lp);
            
            dialog.Show();

            this.Alpha = 1;
            this.GetChildAt(0).Alpha = 1;
        }

        private async Task StartDisappearing()
        {
            ((Activity) this.Context).RunOnUiThread(() =>
            {
                dialog?.Hide();
//                this.RemoveFromParent();
            });
        }

        private int GetNavigationBarHeight()
        {
            var resources = Context.Resources;

            int id = resources.GetIdentifier(
                resources.Configuration.Orientation == global::Android.Content.Res.Orientation.Portrait
                    ? "navigation_bar_height"
                    : "navigation_bar_height_landscape",
                "dimen", "android");
            if (id > 0)
            {
                return resources.GetDimensionPixelSize(id);
            }
            return 0;
        }
    }

    public class BottomSheetDialogEx : BottomSheetDialog{
        protected BottomSheetDialogEx(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public BottomSheetDialogEx(Context context) : base(context)
        {
        }

        protected BottomSheetDialogEx(Context context, bool cancelable, IDialogInterfaceOnCancelListener cancelListener) : base(context, cancelable, cancelListener)
        {
        }

        public BottomSheetDialogEx(Context context, int theme) : base(context, theme)
        {
        }

        public override void OnBackPressed()
        {
            if (OnBackPressing != null)
            {
                if (OnBackPressing())
                    return;
            }
            base.OnBackPressed();
        }

        public Func<bool> OnBackPressing { get; set; }
    }
}