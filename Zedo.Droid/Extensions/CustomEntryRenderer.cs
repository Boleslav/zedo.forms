﻿using System.ComponentModel;
using Android.Content;
using Android.Graphics;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Zedo.Core.Forms.Extensions;
using Zedo.Droid.Extensions;

[assembly: ExportRenderer(typeof(SettingsCellEntry), typeof(CustomEntryRenderer))]
namespace Zedo.Droid.Extensions
    {
        public class CustomEntryRenderer : EntryRenderer
        {
            public CustomEntryRenderer(Context context) : base(context)
            {

            }

            protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
            {
                base.OnElementChanged(e);

                var view = (SettingsCellEntry)Element;
                if (Control != null && view != null)
                {
                    Control.SetHintTextColor(view.PlaceholderColor.ToAndroid());
                    Control.SetTextColor(view.TextColor.ToAndroid());
                    if (!string.IsNullOrWhiteSpace(view.FontFamily))
                    {
                        Control.Typeface = Typeface.CreateFromAsset(Context.Assets, view.FontFamily.Split('#')[0]); 
                    }
//                    Control.Background.SetColorFilter(view.BorderColor.ToAndroid(), PorterDuff.Mode.SrcOut);
//                    Control.Background.SetColorFilter(view.BorderActiveColor.ToAndroid(), PorterDuff.Mode.SrcIn);
                }
            }

            protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
            {
                base.OnElementPropertyChanged(sender, e);
            }
        }
    }
