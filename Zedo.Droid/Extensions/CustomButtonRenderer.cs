﻿using Xamarin.Forms;
using Zedo.Droid.Extensions;

[assembly: ExportRenderer(typeof(Xamarin.Forms.Button), typeof(CustomButtonRenderer))]
namespace Zedo.Droid.Extensions
{
    public class CustomButtonRenderer : Xamarin.Forms.Platform.Android.ButtonRenderer
    {
    }
}
