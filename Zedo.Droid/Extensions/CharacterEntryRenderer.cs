﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using Android.Content;
using Android.Graphics;
using Android.Views;
using Android.Widget;
using Java.Util;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Zedo.Core.Forms.Controls;
using Zedo.Core.Forms.Extensions;
using Zedo.Droid.Extensions;
using View = Android.Views.View;

[assembly: ExportRenderer(typeof(CharacterEntry), typeof(CharacterEntryRenderer))]
namespace Zedo.Droid.Extensions
    {
        public class CharacterEntryRenderer : FrameRenderer
        {
            public CharacterEntryRenderer(Context context) : base(context)
            {

            }

            protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
            {
                base.OnElementChanged(e);

                if (e.NewElement != null)
                {
                    var Control = (CharacterEntry)e.NewElement;
                    Task.Run(async () =>
                    {
                        await Task.Delay(100);
                        var entry = GetEntry<EditText>(this);
                        if (entry != null)
                        {
                            entry.FocusChange += (sender, args) =>
                            {
                                if (!args.HasFocus)
                                {
                                    Control.Blur();
                                }
                            };

                            Control.OnFocusRequested += (sender, args) =>
                            {
                                entry.SetSelection(entry.Text.Length);
                            };
                        }
                    });
                }


                /*
    
                                var view = (SettingsCellEntry)Element;
                                if (ResourceBundle.Control != null && view != null)
                                {
                                    ResourceBundle.Control.SetHintTextColor(view.PlaceholderColor.ToAndroid());
                                    ResourceBundle.Control.SetTextColor(view.TextColor.ToAndroid());
                                    if (!string.IsNullOrWhiteSpace(view.FontFamily))
                                    {
                                        ResourceBundle.Control.Typeface = Typeface.CreateFromAsset(Context.Assets, view.FontFamily.Split('#')[0]); 
                                    }
                //                    Control.Background.SetColorFilter(view.BorderColor.ToAndroid(), PorterDuff.Mode.SrcOut);
                //                    Control.Background.SetColorFilter(view.BorderActiveColor.ToAndroid(), PorterDuff.Mode.SrcIn);
                                }*/
        }

        private T GetEntry<T>(View view) where T : View
        {
            if (view is T)
            {
                return (T)view;
            }

            if (view is ViewGroup)
            {
                var vg = (ViewGroup) view;
                for (int i = 0; i < vg.ChildCount; i++)
                {
                    var v = vg.GetChildAt(i);
                    var r = GetEntry<T>(v);
                    if (r != null)
                    {
                        return r;
                    }
                }
            }

            return null;
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
            {
                base.OnElementPropertyChanged(sender, e);
            }
        }
    }
