using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.Forms.Platforms.Android.Views;
using Zedo.Core;
using Zedo.Core.Forms;

namespace Zedo.Droid
{
    [Activity(
        Label = "Zedo.Dev"
        , MainLauncher = true
        , Icon = "@drawable/icon"
//        , Theme = "@style/MainTheme"
        , Theme = "@style/Theme.Splash"
        , NoHistory = true
        , ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashScreen : MvxFormsSplashScreenAppCompatActivity<Setup, CoreApp, App>
    {
        public SplashScreen()
            : base(Resource.Layout.SplashScreen)
        {
        }

        protected override void RunAppStart(Bundle bundle)
        {
//            StartActivity(typeof(FormsActivity));
            var intent = new Intent(this, typeof(FormsActivity));
            if (Intent.Extras != null)
                intent.PutExtras(Intent.Extras); // copy push info from splash to main
            StartActivity(intent);

            base.RunAppStart(bundle);
        }
    }
}
