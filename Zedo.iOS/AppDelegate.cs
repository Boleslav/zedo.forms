﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using Microsoft.AppCenter.Push;
using MvvmCross;
using MvvmCross.Forms.Platforms.Ios.Core;
using MvvmCross.Platforms.Ios.Core;
using MvvmCross.ViewModels;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Zedo.Core;
using Zedo.Core.Forms;

namespace Zedo.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : MvxFormsApplicationDelegate<Setup, CoreApp, App>
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            InitializeAppearance();
            ZXing.Net.Mobile.Forms.iOS.Platform.Init();
            //            Window = new UIWindow(UIScreen.MainScreen.Bounds);

            //            var setup = new Setup();
            //            setup.InitializePrimary();
            //            setup.Initialize();

            //            var startup = Mvx.Resolve<IMvxAppStart>();
            //            startup.Start();
            //            var setup = MvxIosSetupSingleton.EnsureSingletonAvailable(this, Window);
            //            setup.EnsureInitialized();
            //                        LoadApplication(setup.FormsApplication);

            //            Window.MakeKeyAndVisible();

            return base.FinishedLaunching(app, options);
        }

        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
        {
            var result = Push.DidReceiveRemoteNotification(userInfo);
            if (result)
            {
                completionHandler?.Invoke(UIBackgroundFetchResult.NewData);
            }
            else
            {
                completionHandler?.Invoke(UIBackgroundFetchResult.NoData);
            }
        }

        private void InitializeAppearance()
        {
            var AccentColor = Color.FromRgb(116, 45, 130).ToUIColor();
            UINavigationBar.Appearance.BackgroundColor = AccentColor;
            UINavigationBar.Appearance.BarTintColor = UIColor.White;// AccentColor;
            UINavigationBar.Appearance.TintColor = UIColor.White;
            UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes
            {
                TextColor = UIColor.White
            });

            // others
            UIProgressView.Appearance.ProgressTintColor = AccentColor;

            UISlider.Appearance.MinimumTrackTintColor = AccentColor;
            UISlider.Appearance.MaximumTrackTintColor = AccentColor;
            UISlider.Appearance.ThumbTintColor = AccentColor;

            UISwitch.Appearance.OnTintColor = AccentColor;

            UITableViewHeaderFooterView.Appearance.TintColor = UIColor.White;

            UITableView.Appearance.SectionIndexBackgroundColor = AccentColor;
            UITableView.Appearance.SeparatorColor = AccentColor;

            UITabBar.Appearance.BackgroundColor = AccentColor;
            UITabBar.Appearance.TintColor = UIColor.White;
            UITabBar.Appearance.BarTintColor = AccentColor;
            UITabBar.Appearance.SelectedImageTintColor = UIColor.White;

            UITextField.Appearance.TintColor = AccentColor;

            UIButton.Appearance.TintColor = UIColor.White;
            UIButton.Appearance.SetTitleColor(UIColor.White, UIControlState.Normal);

        }
    }
}
