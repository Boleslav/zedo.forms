﻿using System;
using System.IO;
using Zedo.Core;
using Zedo.Core.Data.Repository;
using Zedo.iOS.Data;

[assembly: Xamarin.Forms.Dependency(typeof(DatabaseAccess))]
namespace Zedo.iOS.Data
{
    public class DatabaseAccess : IDataBaseAccess
    {
        public string DatabasePath()
        {
            string docFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libFolder = Path.Combine(docFolder, "..", "Library", "Databases");

            if (!Directory.Exists(libFolder))
            {
                Directory.CreateDirectory(libFolder);
            }

            return Path.Combine(libFolder, CoreApp.OfflineDatabaseName);
        }
    }
}