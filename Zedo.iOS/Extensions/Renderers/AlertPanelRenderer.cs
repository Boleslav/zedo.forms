﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using CoreGraphics;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Zedo.Core.Forms.Controls;
using Zedo.iOS.Extensions.Renderers;

[assembly:ExportRenderer (typeof(AlertPanel), typeof(AlertPanelRenderer))]
namespace Zedo.iOS.Extensions.Renderers
{
    public class AlertPanelRenderer : FrameRenderer
    {
        private AlertPanel control;
        private UIView container;

        protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
        {
            base.OnElementChanged(e);

            RemoveElement();

            // supporting the only active element through the UI at the moment
            control = e.NewElement as AlertPanel;

            if (control != null)
            {
                control.OnAppearing = StartAppearing;
                control.OnDisappearing = StartDisappearing;
            }
        }

        private void StartAppearing()
        {
            Task.Run(async () =>
            {
                await Task.Delay(10);
                InvokeOnMainThread(() =>
                {
                    CreateElement();

                    SetupLayout();
                    Subviews[0].Alpha = 1;
                    Alpha = 1;
                    SetupAnimationIn();
                });
            });
        }

        private async Task StartDisappearing()
        {
            await HideAnimated();

            Task.Run(async () =>
            {
                await Task.Delay(10);

                InvokeOnMainThread(() =>
                {
                    RemoveElement();

                    container.Hidden = true;
                    container.RemoveFromSuperview();
                });
            });

        }

        private void RemoveElement()
        {
            RemoveFromSuperview();
        }

        private void CreateElement()
        {
            var window = UIApplication.SharedApplication.Delegate.GetWindow();
            var parent = window.RootViewController.View;
            container = new UIView();
            parent.AddSubview(container);
//            var bckView = new UIView(container.Frame);
//            bckView.AddGestureRecognizer(new UITapGestureRecognizer(() =>
//            {
//                control.IsVisible = false;
//            }));
//            bckView.UserInteractionEnabled = true;
//            container.AddSubview(bckView);
            container.AddSubview(this);

            //            container.BackgroundColor = UIColor.Red;
            container.BackgroundColor = Color.FromRgba(0, 200, 0, 0).ToUIColor();

            SetupLayout();
//            container.Hidden = true;
        }

        private void SetupAnimationIn()
        {
            BackgroundColor = Color.FromRgba(0, 0, 0, 0).ToUIColor();
            var window = UIApplication.SharedApplication.Delegate.GetWindow();
            var parent = window.RootViewController.View;
            var childFrame = Subviews[0].Frame;
            Subviews[0].Frame = new CGRect(0, parent.Frame.Height, childFrame.Width, childFrame.Height);
            Animate(.4, 0, UIViewAnimationOptions.CurveEaseIn, () =>
            {
                BackgroundColor = Color.FromRgba(0, 0, 0, .6).ToUIColor();
                Subviews[0].Frame = new CGRect(0, parent.Frame.Height - childFrame.Height, childFrame.Width, childFrame.Height);
            }, null);
        }

        private async Task HideAnimated()
        {
            var animationDone = new Task(() =>
            {
            });

            InvokeOnMainThread(() =>
            {
                var window = UIApplication.SharedApplication.Delegate.GetWindow();
                var parent = window.RootViewController.View;
                var childFrame = Subviews[0].Frame;

                Animate(.4, 0, UIViewAnimationOptions.CurveEaseOut, () =>
                {
                    BackgroundColor = Color.FromRgba(0, 0, 0, 0).ToUIColor();
                    Subviews[0].Frame = new CGRect(0, parent.Frame.Height, childFrame.Width, childFrame.Height);
                },
                () =>
                {
                    animationDone.RunSynchronously();
                });
            });

            await Task.WhenAll(animationDone);
        }

        private void SetupLayout()
        {
            var window = UIApplication.SharedApplication.Delegate.GetWindow();
            var parent = window.RootViewController.View;

            container.Frame = new CGRect(0, 0, parent.Frame.Width, parent.Frame.Height);
            Frame = new CGRect(0, 0, parent.Frame.Width, parent.Frame.Height);
//            container.Subviews[0].Frame = Frame;

            var childFrame = Subviews[0].Frame;
            Subviews[0].Frame = new CGRect(0, parent.Frame.Height - childFrame.Height, childFrame.Width, childFrame.Height);
//            Subviews[0].Frame = new CGRect((Frame.Width - childFrame.Width) / 2, (Frame.Height - childFrame.Height) / 2, childFrame.Width, childFrame.Height);
        }
    }
}