﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using CoreAnimation;
using Foundation;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Zedo.Core.Forms.Controls;
using Zedo.Core.Forms.Extensions;
using Zedo.iOS.Extensions;
using Color = Xamarin.Forms.Color;

[assembly: ExportRenderer(typeof(CharacterEntry), typeof(CharacterEntryRenderer))]
namespace Zedo.iOS.Extensions
{
    public class CharacterEntryRenderer : FrameRenderer
    {
        private CharacterEntry Control;
        protected override void OnElementChanged(ElementChangedEventArgs<Frame> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                Control = (CharacterEntry)e.NewElement;
                var entry = GetEntry<UITextField>(Subviews);
                if (entry != null)
                {
                    entry.Ended += Entry_Ended;
                    entry.EditingDidEnd += Entry_EditingDidEnd;

                    Control.OnFocusRequested += (sender, args) =>
                    {
                        //                        entry.BecomeFirstResponder();
                    };
                }
            }
            

            /*var view = (SettingsCellEntry) Element;
            if (Control != null && view != null)
            {
                if (!isInitialized)
                {
                    isInitialized = true;
                    Control.EditingDidBegin += (sender, args) =>
                    {
                        SetBorderActive(true, view);
                    };
                    Control.EditingDidEnd += (sender, args) =>
                    {
                        SetBorderActive(false, view);
                    };
                }
                SetFont(view);
                SetTextColor(view);
                DrawBorder(view);
            }*/
        }

        private void Entry_EditingDidEnd(object sender, EventArgs e)
        {
            Control.Blur();
        }

        private void Entry_Ended(object sender, EventArgs e)
        {
            Control.Blur();
        }

        private T GetEntry<T>(IEnumerable<UIView> subviews) where T : UIView
        {
            foreach (var view in subviews)
            {
                if (view is T)
                {
                    return (T)view;
                }
                else
                {
                    var viewInChildren = GetEntry<T>(view.Subviews);
                    if (viewInChildren != null)
                    {
                        return viewInChildren;
                    }
                }
            }

            return null;
        }

/*        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            var view = (SettingsCellEntry)Element;
            DrawBorder(view);
            if (e.PropertyName.Equals(nameof(view.BorderColor)))
                DrawBorder(view);
            if (e.PropertyName.Equals(nameof(view.FontSize)))
                SetFont(view);
            if (e.PropertyName.Equals(nameof(view.FontFamily)))
                SetFont(view);
            if (e.PropertyName.Equals(nameof(view.PlaceholderColor)))
                SetTextColor(view);
            if (e.PropertyName.Equals(nameof(view.TextColor)))
                SetTextColor(view);
        }

        void DrawBorder(SettingsCellEntry view)
        {
            if (_border != null)
            {
                return;
            }

            // need a moment for XF to initialize the bounds of native control (not sure why)
            Task.Run(() =>
            {
                // only on main thread!
                InvokeOnMainThread(() =>
                {
                    Control.BorderStyle = UITextBorderStyle.None;

                    _border = new CALayer
                    {
                        MasksToBounds = true,
                        Frame = new CoreGraphics.CGRect(0f, Frame.Height - 1, Frame.Width, 1f),
                        BorderColor = view.BorderColor.ToCGColor(),
                        BorderWidth = 1.0f,
                        CornerRadius = 0
                    };

                    if (Control.Layer.Sublayers != null)
                        foreach (var l in Control.Layer.Sublayers)
                        {
                            l.RemoveFromSuperLayer();
                        }

                    Control.Layer.AddSublayer(_border);
                });
            });
        }

        void SetFont(SettingsCellEntry view)
        {
            var fontSize = view.FontSize != Font.Default.FontSize ?
                (nfloat)view.FontSize
                : Control.Font.PointSize;

            Control.Font = string.IsNullOrEmpty(view.FontFamily)
                ? UIFont.SystemFontOfSize(fontSize)
                : UIFont.FromName(view.FontFamily, fontSize);
        }

        void SetTextColor(SettingsCellEntry view)
        {
            if (string.IsNullOrEmpty(view.Placeholder) == false && view.PlaceholderColor != Color.Default)
            {
                var placeholderString = new NSAttributedString(view.Placeholder,
                                            new UIStringAttributes { ForegroundColor = view.PlaceholderColor.ToUIColor() });
                Control.AttributedPlaceholder = placeholderString;
            }
            Control.TextColor = view.TextColor.ToUIColor();
        }*/
    }
}