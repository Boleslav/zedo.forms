using System.Collections.Generic;
using System.Reflection;
using CarouselView.FormsPlugin.iOS;
using MvvmCross.Forms.Core;
using MvvmCross.Forms.Platforms.Ios.Core;
using MvvmCross.Forms.Presenters;
using MvvmCross.Logging;
using MvvmCross.Platforms.Ios.Core;
using MvvmCross.Platforms.Ios.Presenters;
using MvvmCross.ViewModels;
using UIKit;
using Zedo.Core;
using Zedo.Core.Forms;
using Zedo.Core.Forms.Pages;
using Zedo.Core.ViewModels;
using Zedo.iOS.Extensions.Presentation;

namespace Zedo.iOS
{
    public class Setup : MvxFormsIosSetup<CoreApp, App>
    {
/*
        protected override IMvxApplication CreateApp()
        {
            return new CoreApp();
        }

        protected override Xamarin.Forms.Application CreateFormsApplication()
        {
            return new App();
        }
*/

        protected override void InitializeFirstChance()
        {
            base.InitializeFirstChance();
            CarouselViewRenderer.Init();
        }

/*
        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }
*/

            
/*
        protected override IMvxLogProvider CreateLogProvider()
        {
            return new DebugTraceProvider();// base.CreateLogProvider();
        }
*/
/*

        public override IEnumerable<Assembly> GetViewAssemblies()
        {
            var list = new List<Assembly>();
            list.AddRange(base.GetViewAssemblies());
            list.Add(typeof(App).GetTypeInfo().Assembly);

            return list;
        }

        public override IEnumerable<Assembly> GetViewModelAssemblies()
        {
            var list = new List<Assembly>();
            list.AddRange(base.GetViewModelAssemblies());
            list.Add(typeof(CoreApp).GetTypeInfo().Assembly);

            return list;
        }
*/

        protected override IMvxFormsPagePresenter CreateFormsPagePresenter(IMvxFormsViewPresenter viewPresenter)
        {
            return new GeneralPresenter(viewPresenter);
//            return base.CreateFormsPagePresenter(viewPresenter);
        }
    }
}
