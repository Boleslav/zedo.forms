using System;
using System.Diagnostics;
using MvvmCross.Logging;

namespace Zedo.iOS
{
    public class DebugTraceProvider : IMvxLogProvider
    {
        IMvxLog log = new DebugTrace();

        public IMvxLog GetLogFor<T>()
        {
            return log;
        }

        public IMvxLog GetLogFor(string name)
        {
            return log;
        }

        public IDisposable OpenNestedContext(string message)
        {
            return null;
        }

        public IDisposable OpenMappedContext(string key, string value)
        {
            return null;
        }
    }
    public class DebugTrace : IMvxLog//Provider//Trace
    {
        public bool Log(MvxLogLevel logLevel, Func<string> messageFunc, Exception exception = null, params object[] args)
        {
            string message = "";
            try
            {
                message = messageFunc == null ? "[-]" : messageFunc();
                Debug.WriteLine(message + ":" + logLevel + ":" + exception?.Message, args);

                return true;
            }
            catch (FormatException)
            {
                return Log(MvxLogLevel.Error, () => "Exception during trace of {0} {1}", null, logLevel, message);
            }
        }

        public bool IsLogLevelEnabled(MvxLogLevel logLevel)
        {
            return true;
        }
    }
}
