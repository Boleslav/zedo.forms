﻿using System;
using Xamarin.Essentials;
using Xamarin.Forms;
using Zedo.Core.Forms.Extensions;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.Forms.Controls
{
    public class AdBanner : StackLayout
    {
        public static readonly BindableProperty AdDataProperty = BindableProperty.Create(
            "AdData",
            typeof(AdItem),
            typeof(AdBanner),
            null,
            propertyChanged: ApplyAdData);

        private static void ApplyAdData(BindableObject control, object value, object newValue)
        {
            var ad = newValue as AdItem;
            var c = (AdBanner) control;
            c._ad = ad;

            if (c._image == null)
            {
                c._image = new Image
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    Aspect = Aspect.AspectFill,
                    HeightRequest = c.HeightRequest
                };

                c.Children.Add(c._image);
            }

            c._image.Source = /*"home_category_malls.jpg";//*/ ad?.Image;
            c.IsVisible = !string.IsNullOrWhiteSpace(ad?.Image);
        }

        private Image _image;
        private AdItem _ad = null;
        public AdBanner()
        {
            HeightRequest = 180;
            HorizontalOptions = LayoutOptions.FillAndExpand;

            var onTap = new TapGestureRecognizer();
            onTap.Tapped += OnBanner_Tapped;
            GestureRecognizers.Add(onTap);
        }

        private void OnBanner_Tapped(object sender, EventArgs e)
        {
            var link = _ad?.Link;
            if (!string.IsNullOrWhiteSpace(link))
            {
                try
                {
                    Browser.OpenAsync(link, BrowserLaunchMode.SystemPreferred);
//                    Device.OpenUri(new Uri(link));
                }
                catch(Exception ex)
                {
                    // nothing to do. just some issue with the link format
                }
            }
        }
    }
}