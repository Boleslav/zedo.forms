﻿using System;
using System.Threading.Tasks;
using MvvmCross.Commands;
using Xamarin.Forms;
using Zedo.Core.Forms.Extensions;

namespace Zedo.Core.Forms.Controls
{
    public class CharacterEntry : Frame
    {
        private AbsoluteLayout _layout;
        private Entry _entry;
        private BoxView _back;
        private Label _label;
        private Frame _content;
        private string _value;

        public event EventHandler<OnValueEnteredArgs> OnValueEntered;
        public event EventHandler OnValueCleaned;
        public event EventHandler OnFocusRequested;

        public static readonly BindableProperty IsNumericOnlyProperty = BindableProperty.Create(
            "IsNumericOnly",
            typeof(bool),
            typeof(CharacterEntry),
            false,
            propertyChanged: (bindable, value, newValue) =>
            {
                ((CharacterEntry) bindable)._entry.Keyboard = (bool) newValue ? Keyboard.Numeric : Keyboard.Text;
            });

        public string Value
        {
            get => _value;
            set
            {
                _value = value;
                _label.Text = value;
            }
        }

        public CharacterEntry()
        {
            SetFocusedState(false);
            Padding = 0;
            Margin = 0;
            CornerRadius = 8;
            HasShadow = false;

            GestureRecognizers.Add(new TapGestureRecognizer()
            {
                Command = new MvxCommand(() =>
                {
                    Task.Run(() =>
                    {
                        Focus();
                    });
                })
            });

            _content = new Frame
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Margin = 2,
                CornerRadius = this.CornerRadius + 2,
                HasShadow = false,
                Padding = 0
            };
            Content = _content;

            _layout = new AbsoluteLayout
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
            };
            _content.Content = _layout;

            _entry = new Entry
            {
                Text = " "
            };
            _entry.TextChanged += _entry_TextChanged;
            _layout.Children.Add(_entry, new Rectangle(.5, .5, -1, -1), AbsoluteLayoutFlags.PositionProportional);

            _back = new BoxView
            {
                BackgroundColor = Color.White
            };
            _layout.Children.Add(_back, new Rectangle(0, 0, 1, 1), AbsoluteLayoutFlags.All);

            _label = new Label
            {
                HorizontalTextAlignment = TextAlignment.Center,
                FontSize = 24,
                TextColor = Color.FromHex("742d82")
            };
            _layout.Children.Add(_label, new Rectangle(.5, .5, -1, -1), AbsoluteLayoutFlags.PositionProportional);
        }

        public string Placeholder { get; set; }
        public bool IsNumericOnly { get; set; }

        private void _entry_TextChanged(object sender, TextChangedEventArgs e)
        {
            var value = _entry.Text;
            if (value.Length > 1)
            {
                value = value.Substring(value.Length - 1);
            }

            _entry.Text = " ";

            if (value.Length > 0)
            {
                if (IsNumericOnly && !char.IsDigit(value[0])
                    || !char.IsLetterOrDigit(value[0]))
                {
                    return;
                }

                _label.Text = value;
                _value = value[0].ToString();
                OnValueEntered?.Invoke(this, new OnValueEnteredArgs {Value = value[0]});
            }
            else
            {
                _label.Text = "";
                _value = "";
                OnValueCleaned?.Invoke(this, new EventArgs());
            }
        }

        private void SetFocusedState(bool isFocused)
        {
            BackgroundColor = isFocused ? Color.FromHex("742d82") : Color.LightGray;
//            CornerRadius = 10;
        }

        public void Blur()
        {
            SetFocusedState(false);
        }

        public new void Focus()
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                SetFocusedState(true);
                _entry.Focus();
                OnFocusRequested?.Invoke(this, new EventArgs());
            });
        }
    }

    public class OnValueEnteredArgs : EventArgs
    {
        public char Value { get; set; }
    }
}