﻿using System;
using System.Collections.Generic;
using MvvmCross.Binding.Extensions;
using Xamarin.Forms;

namespace Zedo.Core.Forms.Extensions
{
    public class StackLayoutRepeater : StackLayout
    {
        public static readonly BindableProperty ItemTemplateProperty = BindableProperty.Create(
            "ItemTemplate",
            typeof(DataTemplate),
            typeof(StackLayoutRepeater),
            null,
            propertyChanged: (bindable, value, newValue) => Populate(bindable));

        public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create(
            "ItemsSource",
            typeof(IEnumerable<object>),
            typeof(StackLayoutRepeater),
            null,
            BindingMode.OneWay,
            propertyChanged: (bindable, value, newValue) => Populate(bindable));

        public IEnumerable<object> ItemsSource
        {
            get => (IEnumerable<object>)this.GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        public DataTemplate ItemTemplate
        {
            get => (DataTemplate)this.GetValue(ItemTemplateProperty);
            set => SetValue(ItemTemplateProperty, value);
        }

        private static void Populate(BindableObject bindable)
        {
            var rpt = (StackLayoutRepeater)bindable;

            // Clean
            rpt.Children.Clear();//.Content = null;

            // Only populate once both properties are recieved
            if (rpt.ItemsSource == null || rpt.ItemTemplate == null || rpt.ItemsSource.Count() == 0)
            {
                return;
            }

            rpt.Children.Clear();

            var itemsCount = rpt.ItemsSource.Count();
            for (var i = 0; i < itemsCount; i++)
            {
                try
                {
                    var viewModel = rpt.ItemsSource.ElementAt(i);
                    var content = rpt.ItemTemplate.CreateContent();
                    if (!(content is View) && !(content is ViewCell))
                    {
                        throw new Exception($"Invalid visual object {nameof(content)}");
                    }

                    var view = content is View ? content as View : ((ViewCell)content).View;
                    view.BindingContext = viewModel;

                    rpt.Children.Add(view);

                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }
        }
    }
}