﻿using System;
using System.Collections;
using MvvmCross.Binding.Extensions;
using Xamarin.Forms;

namespace Zedo.Core.Forms.Extensions
{
    public class GridRepeater : Grid
    {
        public static readonly BindableProperty ColumnsCountProperty = BindableProperty.Create(
            "ColumnsCount",
            typeof(int),
            typeof(GridRepeater),
            2,
            propertyChanged: (bindable, value, newValue) => Populate(bindable));

        public static readonly BindableProperty RowHeightRequestProperty = BindableProperty.Create(
            "RowHeightRequest",
            typeof(int),
            typeof(GridRepeater),
            75,
            propertyChanged: (bindable, value, newValue) => Populate(bindable));

        public static readonly BindableProperty ItemTemplateProperty = BindableProperty.Create(
            "ItemTemplate",
            typeof(DataTemplate),
            typeof(GridRepeater),
            null,
            propertyChanged: (bindable, value, newValue) => Populate(bindable));

        public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create(
            "ItemsSource",
            typeof(IEnumerable),
            typeof(GridRepeater),
            null,
            BindingMode.OneWay,
            propertyChanged: (bindable, value, newValue) => Populate(bindable));

        public int ColumnsCount
        {
            get => (int)this.GetValue(ColumnsCountProperty);
            set => SetValue(ColumnsCountProperty, value);
        }

        public int RowHeightRequest
        {
            get => (int)this.GetValue(RowHeightRequestProperty);
            set => SetValue(RowHeightRequestProperty, value);
        }

        public IEnumerable ItemsSource
        {
            get => (IEnumerable)this.GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        public DataTemplate ItemTemplate
        {
            get => (DataTemplate)this.GetValue(ItemTemplateProperty);
            set => SetValue(ItemTemplateProperty, value);
        }

        private static void Populate(BindableObject bindable)
        {
            var grid = (GridRepeater)bindable;

            // Clean
            grid.Children.Clear();//.Content = null;

            // Only populate once both properties are recieved
            if (grid.ItemsSource == null || grid.ItemTemplate == null || grid.ItemsSource.Count() == 0)
            {
                return;
            }

            grid.RowDefinitions.Clear();
            grid.ColumnDefinitions.Clear();

            for (int i = 0; i < grid.ColumnsCount; i++)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition() {Width = GridLength.Star});
            }

            var itemsCountForColumnCalculation = grid.ItemsSource.Count();
            itemsCountForColumnCalculation += itemsCountForColumnCalculation > 0 ? -1 : 0;
            var rowsCount = itemsCountForColumnCalculation / grid.ColumnsCount + 1;
            for (int i = 0; i < rowsCount; i++)
            {
                grid.RowDefinitions.Add(new RowDefinition(){ Height = grid.RowHeightRequest});
            }

            // Create a stack to populate with items
//            var list = new StackLayout();

            var itemsCount = grid.ItemsSource.Count();
            for(var i = 0; i < itemsCount; i++)
            {
                var viewModel = grid.ItemsSource.ElementAt(i);
                var content = grid.ItemTemplate.CreateContent();
                if (!(content is View) && !(content is ViewCell))
                {
                    throw new Exception($"Invalid visual object {nameof(content)}");
                }

                var view = content is View ? content as View : ((ViewCell)content).View;
                view.BindingContext = viewModel;

                var column = i % grid.ColumnsCount;
                var row = i / grid.ColumnsCount;
                grid.Children.Add(view, column, row);
            }
        }
    }
}