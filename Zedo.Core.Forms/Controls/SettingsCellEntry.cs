﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Zedo.Core.Forms.Extensions
{
    public class SettingsCellEntry : Entry
    {
        public static readonly BindableProperty BorderColorProperty =
            BindableProperty.Create<SettingsCellEntry, Color>(p => p.BorderColor, Color.Black);

        public Color BorderColor
        {
            get { return (Color)GetValue(BorderColorProperty); }
            set { SetValue(BorderColorProperty, value); }
        }

        public static readonly BindableProperty BorderActiveColorProperty =
            BindableProperty.Create<SettingsCellEntry, Color>(p => p.BorderActiveColor, Color.Black);

        public Color BorderActiveColor
        {
            get { return (Color)GetValue(BorderActiveColorProperty); }
            set { SetValue(BorderActiveColorProperty, value); }
        }

        public static readonly BindableProperty FontSizeProperty =
            BindableProperty.Create<SettingsCellEntry, double>(p => p.FontSize, Font.Default.FontSize);

        public double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        public static readonly BindableProperty TextColorProperty =
            BindableProperty.Create<SettingsCellEntry, Color>(p => p.TextColor, Color.Gray);

        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }

        public static readonly BindableProperty FontFamilyProperty =
            BindableProperty.Create<SettingsCellEntry, string>(p => p.FontFamily, Font.Default.FontFamily);

        public string FontFamily
        {
            get { return (string)GetValue(FontFamilyProperty); }
            set { SetValue(FontFamilyProperty, value); }
        }

        public static readonly BindableProperty PlaceholderColorProperty =
            BindableProperty.Create<SettingsCellEntry, Color>(p => p.PlaceholderColor, Color.Default);

        public Color PlaceholderColor
        {
            get { return (Color)GetValue(PlaceholderColorProperty); }
            set { SetValue(PlaceholderColorProperty, value); }
        }

        public SettingsCellEntry()
        {
            var rEntry = Application.Current.Resources["Xamarin.Forms.Entry"];
            ApplyStyle(rEntry as Style);
            var resource = Application.Current.Resources["SettingsCellEntry"] as Style;
            if (resource == null)
            {
                throw new Exception("SettingsCellEntry Style is not defined for control use.");
            }
            ApplyStyle(resource);
        }

        private void ApplyStyle(Style resource)
        {
            if (resource == null)
            {
                return;
            }

            foreach (var s in resource.Setters)
            {
                if (s.Property.PropertyName == "FontFamily")
                {
                    FontFamily = s.Value as string;
                }
                else if (s.Property.PropertyName == "FontSize")
                {
                    FontSize = (int)s.Value;
                }
                else if (s.Property.PropertyName == "TextColor")
                {
                    TextColor = (Color)s.Value;
                }
                else if (s.Property.PropertyName == "PlaceholderColor")
                {
                    PlaceholderColor = (Color)s.Value;
                }
                else if (s.Property.PropertyName == "Margin")
                {
                    Margin = (Thickness)s.Value;
                }
            }
        }
    }
}