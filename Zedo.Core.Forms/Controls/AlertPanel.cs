﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Zedo.Core.Forms.Controls
{
    public class AlertPanel : Frame
    {
        public AlertPanel()
        {
//            BackgroundColor = Color.FromRgba(0, 0, 0, .5);
            HasShadow = false;
            CornerRadius = 0;
            Padding = 0;

//            Opacity = 0;
        }

        public new static BindableProperty IsVisibleProperty =
            BindableProperty.Create("IsVisible", typeof(bool), typeof(AlertPanel), true, BindingMode.TwoWay);

        private bool _isVisible;
        public new bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                if (IsTransitioning || value == _isVisible)
                {
                    return;
                }

                _isVisible = value;

                if (value) // start appearing
                {
                    StartAppearing();
                }
                else
                {
                    StartDisappearing();
                }
            }
        }

        public Action OnAppearing { get; set; }
        public Func<Task> OnDisappearing { get; set; }
        public Action OnDisappeared { get; set; }

        // it means it's disposing
        public bool IsTransitioning { get; set; }

        private void StartAppearing()
        {
            base.IsVisible = true;

            if (OnAppearing != null)
            {
//                Opacity = 0;
                IsTransitioning = true;

                OnAppearing();
                // to be called inside of OnAppearing due to async operations implementation complicity
                IsTransitioning = false;
            }
        }

        private async void StartDisappearing()
        {
            if (OnDisappearing != null)
            {
                IsTransitioning = true;
                await Task.Run(OnDisappearing);
                IsTransitioning = false;
                base.IsVisible = false;
            }
            else
            {
                base.IsVisible = false;
            }
        }
    }
}