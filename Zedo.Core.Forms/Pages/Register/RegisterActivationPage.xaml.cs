﻿using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Zedo.Core.ViewModels;

namespace Zedo.Core.Forms.Pages
{
    public partial class RegisterActivationPage : MvxContentPage<RegisterActivationViewModel>
    {
        public RegisterActivationPage()
        {
            NavigationPage.SetHasBackButton(this, false);

            InitializeComponent();
        }
    }
}
