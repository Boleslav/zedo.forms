﻿using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Zedo.Core.ViewModels;

namespace Zedo.Core.Forms.Pages
{
    public partial class RegisterPage : MvxContentPage<RegisterViewModel>
    {
        public RegisterPage()
        {
            NavigationPage.SetBackButtonTitle(this, "");

            InitializeComponent();
        }
    }
}
