﻿using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Zedo.Core.ViewModels;

namespace Zedo.Core.Forms.Pages
{
    public partial class RegisterConfirmPage : MvxContentPage<RegisterConfirmViewModel>
    {
        public RegisterConfirmPage()
        {
            NavigationPage.SetBackButtonTitle(this, "");

            InitializeComponent();
        }
    }
}
