﻿using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Zedo.Core.ViewModels;

namespace Zedo.Core.Forms.Pages
{
    public partial class RegisterTermsPage : MvxContentPage<RegisterTermsViewModel>
    {
        public RegisterTermsPage()
        {
            NavigationPage.SetBackButtonTitle(this, "");

            InitializeComponent();
        }
    }
}
