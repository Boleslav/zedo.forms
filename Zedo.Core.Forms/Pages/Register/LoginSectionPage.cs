﻿using System;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using MvvmCross.Presenters;
using MvvmCross.Presenters.Attributes;
using MvvmCross.ViewModels;
using Xamarin.Forms;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.Forms.Pages.Account
{
    public partial class LoginSectionPage : MvxNavigationPage<LoginSectionViewModel>, IMvxOverridePresentationAttribute
    {
        public LoginSectionPage()
        {
            NavigationPage.SetHasNavigationBar(this, true);
        }
        private bool _firstTime = true;

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (_firstTime)
            {
                //ViewModel.ShowInitialViewModelsCommand.Execute();
                ViewModel.ShowInitialViewModelsCommand.ExecuteAsync(null);
                _firstTime = false;
            }
        }

        public MvxBasePresentationAttribute PresentationAttribute(MvxViewModelRequest request)
        {
            if (request.PresentationValues != null)
            {
                if (request.PresentationValues.ContainsKey("NavigationMode") &&
                    request.PresentationValues["NavigationMode"] == "Modal")
                {
                    return new MvxModalPresentationAttribute() { };
                }
            }

            return null;
        }

    }
}
