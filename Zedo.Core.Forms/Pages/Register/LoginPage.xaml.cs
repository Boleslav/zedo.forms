﻿using System;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using MvvmCross.Presenters;
using MvvmCross.Presenters.Attributes;
using MvvmCross.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Zedo.Core.ViewModels;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.Forms.Pages
{
    [MvxContentPagePresentation(WrapInNavigationPage = true, NoHistory = true, Title = "Sign In")]
    public partial class LoginPage : MvxContentPage<LoginViewModel>, IMvxOverridePresentationAttribute
    {
        public LoginPage()
        {
            NavigationPage.SetBackButtonTitle(this, "");
            NavigationPage.SetHasNavigationBar(this, false);

            Title = "Sign In";

            InitializeComponent();

            ToolbarItems.ForEach(x => x.Icon = null);
        }

        protected override void OnViewModelSet()
        {
            NavigationPage.SetHasNavigationBar(this, ViewModel?.IsModal == true);
        }

        public MvxBasePresentationAttribute PresentationAttribute(MvxViewModelRequest request)
        {
            if (request.PresentationValues != null)
            {
                if (request.PresentationValues.ContainsKey("NavigationMode") &&
                    request.PresentationValues["NavigationMode"] == "Modal")
                {
                    return new MvxModalPresentationAttribute
                    {
                        WrapInNavigationPage = true, NoHistory = true
                    };
                }
            }

            return null;
        }

    }

    public class CustomPresentationAttribute : Attribute, IMvxOverridePresentationAttribute
    {
        public MvxBasePresentationAttribute PresentationAttribute(MvxViewModelRequest request)
        {
            return new MvxModalPresentationAttribute();
        }
    }
}
