﻿using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Zedo.Core.ViewModels;

namespace Zedo.Core.Forms.Pages
{
    public partial class ForgotPasswordPage : MvxContentPage<ForgotPasswordViewModel>
    {
        public ForgotPasswordPage()
        {
            NavigationPage.SetBackButtonTitle(this, "");

            InitializeComponent();
        }
    }
}
