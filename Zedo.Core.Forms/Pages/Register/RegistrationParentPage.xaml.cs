﻿using System;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using MvvmCross.Presenters;
using MvvmCross.Presenters.Attributes;
using MvvmCross.ViewModels;
using Xamarin.Forms;
using Zedo.Core.ViewModels;

namespace Zedo.Core.Forms.Pages
{
    [MvxModalPresentation]
    public partial class RegistrationParentPage : MvxNavigationPage<RegistrationParentViewModel>
    {
        public RegistrationParentPage()
        {
            NavigationPage.SetBackButtonTitle(this, "");
            NavigationPage.SetHasNavigationBar(this, false);

            InitializeComponent();
        }
    }
}
