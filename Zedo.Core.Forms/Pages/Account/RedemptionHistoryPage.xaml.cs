﻿using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.Forms.Pages.Account
{
    [MvxContentPagePresentation(WrapInNavigationPage = true, NoHistory = false, Title = "Redemption History")]
    public partial class RedemptionHistoryPage : MvxContentPage<RedemptionHistoryViewModel>
    {
        public RedemptionHistoryPage()
        {
            NavigationPage.SetHasNavigationBar(this, true);

            InitializeComponent();
        }

        private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            lstItems.SelectedItem = null;
        }
    }
}
