﻿using System;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.Forms.Pages.Account
{
    [MvxContentPagePresentation(WrapInNavigationPage = true, Title = "Account", Icon = "menu_supervisor_account")]
    public partial class AccountSectionPage : MvxContentPage<AccountSectionViewModel>
    {
        public AccountSectionPage()
        {
            NavigationPage.SetHasNavigationBar(this, true);

            Title = "Account";

            InitializeComponent();
        }
    }
}
