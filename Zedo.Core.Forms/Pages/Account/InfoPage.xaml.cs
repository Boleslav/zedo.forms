﻿using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.Forms.Pages.Account
{
    public partial class InfoPage : MvxContentPage<InfoViewModel>
    {
        public InfoPage()
        {
            NavigationPage.SetHasNavigationBar(this, true);

            Title = "About Zedo's";

            InitializeComponent();
        }
    }
}
