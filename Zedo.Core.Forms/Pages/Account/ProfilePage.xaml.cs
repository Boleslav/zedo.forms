﻿using System;
using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.Forms.Pages.Account
{
    public partial class ProfilePage : MvxContentPage<ProfileViewModel>
    {
        public ProfilePage()
        {
            NavigationPage.SetHasNavigationBar(this, true);

            Title = "My Profile";

            InitializeComponent();
        }

    }
}
