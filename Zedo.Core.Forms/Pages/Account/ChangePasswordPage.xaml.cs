﻿using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.Forms.Pages.Account
{
    public partial class ChangePasswordPage : MvxContentPage<ChangePasswordViewModel>
    {
        public ChangePasswordPage()
        {
            NavigationPage.SetHasNavigationBar(this, true);

            Title = "Change Password";

            InitializeComponent();
        }
    }
}
