﻿using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Zedo.Core.ViewModels;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.Forms.Pages.Main
{
    [MvxContentPagePresentation(WrapInNavigationPage = true, Title = "Favorites", Icon = "menu_star_rate")]
    public partial class FavoritesPage : MvxContentPage<FavoritesViewModel>
    {
        public FavoritesPage()
        {
            NavigationPage.SetHasNavigationBar(this, true);

            Title = "Favorites";

            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            lstItems.SelectedItem = null;
        }
    }
}
