﻿using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Zedo.Core.ViewModels;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.Forms.Pages.Main
{
    public partial class PortalAreaBasePage : MvxNavigationPage<PortalAreaViewModel>
    {
        public PortalAreaBasePage(Page rootPage)// : base(rootPage)
        {
            NavigationPage.SetHasNavigationBar(this, true);

            PushAsync(rootPage);


            //            NavigationPage.SetBackButtonTitle(this, "Home");
            //            NavigationPage.SetHasNavigationBar(this, true);

            //            InitializeComponent();
        }

        protected override bool OnBackButtonPressed()
        {
            if (Navigation.NavigationStack.Count <= 1)
            {
                return true;
            }

            return base.OnBackButtonPressed();
        }

        protected override void OnChildAdded(Element child)
        {
            base.OnChildAdded(child);
        }

        protected override void OnChildRemoved(Element child)
        {
            base.OnChildRemoved(child);
        }
    }
}
