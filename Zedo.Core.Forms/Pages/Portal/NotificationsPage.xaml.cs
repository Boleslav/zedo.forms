﻿using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Zedo.Core.ViewModels;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.Forms.Pages.Main
{
    [MvxContentPagePresentation(WrapInNavigationPage = true, Title = "Notifications", Icon = "menu_notifications")]
    public partial class NotificationsPage : MvxContentPage<NotificationsViewModel>
    {
        public NotificationsPage()
        {
            NavigationPage.SetHasNavigationBar(this, true);

            Title = "Notifications";

            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            lstItems.SelectedItem = null;
        }


    }
}
