﻿using System;
using System.Threading.Tasks;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Zedo.Core.Forms.Controls;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.Forms.Pages.Portal.Home
{
    public partial class StoresListPage : MvxContentPage<StoresListViewModel>
    {
        private bool _isFilterVisible;

        public StoresListPage()
        {
//            NavigationPage.SetBackButtonTitle(this, "Malls");
//            NavigationPage.SetHasNavigationBar(this, true);

            InitializeComponent();

            pnlFilters.OnDisappeared = OnFilterDisappeared;
        }

        private void OnFilterDisappeared()
        {
            IsFilterVisible = false;
            if (ViewModel != null)
                ViewModel.IsFilterVisible = false;
        }

        protected override void OnViewModelSet()
        {
            base.OnViewModelSet();

            Title = ViewModel?.Title;

            var set = this.CreateBindingSet<StoresListPage, StoresListViewModel>();
            set.Bind(this).For(x => x.IsFilterVisible).To(vm => vm.IsFilterVisible);
            set.Apply();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            lstItems.SelectedItem = null;
        }

        public bool IsFilterVisible
        {
            get => _isFilterVisible;
            set
            {
                _isFilterVisible = value;
                pnlFilters.IsVisible = value;
            }
        }

        protected void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            if (args.SelectedItem != null)
            {
                ViewModel.GoSelectItem.Execute(args.SelectedItem);
            }
        }

/*
        protected override bool OnBackButtonPressed()
        {
            if (pnlFilters.IsVisible && Device.RuntimePlatform == Device.Android)
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    IsFilterVisible = false;
                });

                return true;
            }

            return base.OnBackButtonPressed();
        }
*/
    }
}
