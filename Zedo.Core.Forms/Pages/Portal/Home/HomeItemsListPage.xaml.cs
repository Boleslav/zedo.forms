﻿using System;
using System.Linq;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.Forms.Pages.Portal.Home
{
    public partial class HomeItemsListPage : MvxContentPage<HomeItemsListViewModel>
    {
        public HomeItemsListPage()
        {
            NavigationPage.SetBackButtonTitle(this, "");
//            NavigationPage.SetHasNavigationBar(this, true);

            InitializeComponent();
        }

        protected override void OnViewModelSet()
        {
            base.OnViewModelSet();

            Title = ViewModel?.Title;

            var set = this.CreateBindingSet<HomeItemsListPage, HomeItemsListViewModel>();
            set.Bind(this)
                .For(v => v.Items)
                .To(vm => vm.Items);
            set.Apply();
        }

        public object Items
        {
            get { return null; }
            set
            {
                if (ViewModel.Items == null)
                {
                    return;
                }

                RefreshListHeight();
            }
        }

        private void RefreshListHeight()
        {
            lstItems.HeightRequest = lstItems.RowHeight * ViewModel.Items?.Count() ?? 0;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            lstItems.SelectedItem = null;
        }

        protected void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            if (args.SelectedItem != null)
            {
                ViewModel.GoSelectItem.Execute(args.SelectedItem);
            }
        }

        private void LstItems_OnSizeChanged(object sender, EventArgs e)
        {
//            pnlListContainer.HeightRequest = pnlListContainer.Children.Sum(x => 
//                x == lstItems ? lstItems.RowHeight * ViewModel.Items?.Count() ?? 0 : x.Height);
        }


    }
}
