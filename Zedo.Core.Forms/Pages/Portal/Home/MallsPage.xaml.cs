﻿using MvvmCross.Forms.Views;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.Forms.Pages.Portal.Home
{
    public partial class MallsPage : MvxContentPage<MallsViewModel>
    {
        public MallsPage()
        {
//            NavigationPage.SetBackButtonTitle(this, "Malls");
//            NavigationPage.SetHasNavigationBar(this, true);

            Title = "Malls";

            InitializeComponent();
        }
    }
}
