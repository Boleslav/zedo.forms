﻿using System;
using System.Linq;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;
using Zedo.Core.Forms.Controls;
using Zedo.Core.ViewModels.Portal;
using ZXing;
using ZXing.Common;
using ZXing.Net.Mobile.Forms;
using ZXing.OneD;
using ZXing.QrCode;

namespace Zedo.Core.Forms.Pages.Portal.Home
{
    [MvxModalPresentation(Animated = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OfferDetailsPage : MvxContentPage<OfferDetailsViewModel>
    {
        private CharacterEntry[] symbols;
        private string _pinCode;

        public OfferDetailsPage()
        {
//            NavigationPage.SetBackButtonTitle(this, "");
            NavigationPage.SetHasNavigationBar(this, false);

            InitializeComponent();

            symbols = new []
            {
                entry0,
                entry1,
                entry2,
                entry3,
            };

            for (int i = 0; i < symbols.Length; i++)
            {
                var entry = symbols[i];
                entry.OnValueCleaned += Entry_OnValueCleaned;
                entry.OnValueEntered += Entry_OnValueEntered;
            }
        }

        private void Entry_OnValueEntered(object sender, EventArgs e)
        {
            var entry = sender as CharacterEntry;
            var currentIndex = symbols.IndexOf(entry);
            var nextIndex = currentIndex + 1;
            symbols[currentIndex].Blur();
            if (nextIndex < symbols.Length)
            {
                symbols[nextIndex].Focus();
            }

            UpdatePinCodeValue();
        }

        private void Entry_OnValueCleaned(object sender, EventArgs e)
        {
            var entry = sender as CharacterEntry;
            var currentIndex = symbols.IndexOf(entry);
            var nextIndex = currentIndex - 1;
            if (nextIndex >= 0)
            {
                symbols[currentIndex].Blur();
                symbols[nextIndex].Focus();
            }

            UpdatePinCodeValue();
        }

        private void UpdatePinCodeValue()
        {
            ViewModel.PinCode = string.Join("", symbols.Select(x => string.IsNullOrWhiteSpace(x.Value) ? " " : x.Value));
        }

        protected override void OnViewModelSet()
        {
            base.OnViewModelSet();

            var set = this.CreateBindingSet<OfferDetailsPage, OfferDetailsViewModel>();
            set.Bind(this).For(x => x.Coupon).To(x => x.Coupon);
            set.Bind(this).For(x => x.PinCode).To(x => x.PinCode);
            set.Apply();
        }

        public string PinCode
        {
            get => _pinCode;
            set
            {
                _pinCode = value ?? "";
                for (int i = 0; i < symbols?.Length; i++)
                {
                    var v = "";
                    if (_pinCode.Length >= symbols.Length)
                    {
                        v = _pinCode[i].ToString();
                    }

                    symbols[i].Value = v;
                }
            }
        }

        public RedeemedCoupon Coupon
        {
            get => null;
            set
            {
                var code = value?.Code;
                code = string.IsNullOrWhiteSpace(code) ? "0" : code;

                BarCodeQr.BarcodeValue = code;
                BarCode39.BarcodeValue = code;
            }
        }

    }
}
