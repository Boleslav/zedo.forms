﻿using System;
using System.Threading.Tasks;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.Forms.Pages.Portal.Home
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class StoreDetailsPage : MvxContentPage<StoreDetailsViewModel>
    {
        public StoreDetailsPage()
        {
            NavigationPage.SetBackButtonTitle(this, "");
//            NavigationPage.SetHasNavigationBar(this, true);

            InitializeComponent();
        }

        protected override void OnViewModelSet()
        {
            base.OnViewModelSet();

            Title = ViewModel.Store?.Title;

            var set = this.CreateBindingSet<StoreDetailsPage, StoreDetailsViewModel>();

            set.Bind(this).For(x => x.IsFavorite).To(vm => vm.IsFavorite);
            set.Bind(this).For(x => x.Title).To(vm => vm.Title);

            set.Apply();
        }

        public bool IsFavorite
        {
            // dummy getter
            get { return false; }
            set
            {
                ToolbarFavoriteItem.Icon = new FileImageSource()
                {
                    File = value ? "icon_favorites_active" : "icon_favorites"
                };
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

//            lstPromos.SelectedItem = null;
        }

        private void ExpiredOfferSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ((ListView) sender).SelectedItem = null;
        }

        private void Offer_OnTapped(object sender, EventArgs e)
        {
            ViewModel?.GoOffer?.Execute(((Frame)sender).BindingContext);

        }

        private void OnCoupon_Tapped(object sender, EventArgs e)
        {
            Task.Run(() =>
            {
                ViewModel.GoOffer.Execute(((StackLayout)sender).BindingContext);
            });
            Console.WriteLine(" = = = = = = = = = = = ");
        }
    }
}
