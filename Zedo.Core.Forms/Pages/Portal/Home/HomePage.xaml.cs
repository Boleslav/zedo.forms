using System;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.Forms.Pages.Portal.Home
{
    [MvxContentPagePresentation(WrapInNavigationPage = true, Title = "ZEDO", Icon = "menu_home")]
//    [MvxTabbedPagePresentation(WrapInNavigationPage = true, Title = "ZEDO", Icon = )]
    public partial class HomePage : MvxContentPage<HomeViewModel>
    {
        public HomePage()
        {
            NavigationPage.SetBackButtonTitle(this, "Home");
            Title = "ZEDO";
            Icon = "menu_home";

            InitializeComponent();

            ViewModel = new HomeViewModel();
            BindingContext = new MvxBindingContext(ViewModel);
        }

        private void Button_OnClicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new MallsPage());
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class MvxContentSectionPagePresentationAttrbute : MvxContentPagePresentationAttribute
    {

    }
}
