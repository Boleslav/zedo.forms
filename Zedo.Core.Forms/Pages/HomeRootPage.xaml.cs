﻿using MvvmCross.Forms.Presenters.Attributes;
using MvxBottomBar.XamarinForms;
using Zedo.Core.ViewModels.Portal;

namespace Zedo.Core.Forms.Pages
{
    [MvxTabbedPagePresentation(TabbedPosition.Root, NoHistory = true, WrapInNavigationPage = false)]
    public partial class HomeRootPage : MvxBottomBarPage<HomeRootViewModel>
    {
        public HomeRootPage()
        {
//            NavigationPage.SetBackButtonTitle(this, "");
//            NavigationPage.SetHasNavigationBar(this, false);

            InitializeComponent();

            BarItemsMeta = new BarItemMeta[]
            {
                new BarItemMeta(), 
                new BarItemMeta(), 
                new BarItemMeta(), 
                new BarItemMeta(), 
            };
//            Initialize();
        }

        private bool _firstTime = true;

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (_firstTime)
            {
                //ViewModel.ShowInitialViewModelsCommand.Execute();
                ViewModel.ShowInitialViewModelsCommand.ExecuteAsync(null);
                _firstTime = false;
            }
        }

        /*
                private void Initialize()
                {
        //            BarBackgroundColor = Color.Pink;

                    var pages = new List<Page>
                    {
                        new PortalAreaBasePage(new HomePage())
                        {
        //                    Title = "",
                            Icon = GetIcon("home"),
                        },
                            new PortalAreaBasePage(new NotificationsPage())
                        {
        //                    Title = "",
                            Icon = GetIcon("notifications")
                        },
                        new PortalAreaBasePage(new FavoritesPage())
                        {
        //                    Title = "",
                            Icon = GetIcon("star_rate")
                        },
                                new PortalAreaBasePage(new AccountSectionPage())
                        {
        //                    Title = "",
                            Icon = GetIcon("supervisor_account")
                        }
                    };

                    pages.ForEach(Children.Add);
                    CurrentPage = pages.FirstOrDefault();

         /*           // You can only define the color for the active icon if you set the Bottombar to fixed mode
                    // So if you want to try this, just uncomment the next two lines

                    //bottomBarPage.BarTextColor = Color.Blue; // Setting Color of selected Text and Icon
                    //bottomBarPage.FixedMode = true;

                    // Whith BarTheme you can select between light and dark theming when using FixedMode
                    // When using DarkTheme you can set the Background Color by adding a colors.xml to you Android.Resources.Values
                    // with content
                    //
                    //  <color name="white">#ffffff</color>
                    //  < color name = "bb_darkBackgroundColor" >#000000</color>
                    //
                    // by setting "white" you can select the color of the non selected items and texts in dark theme
                    // The Difference between DarkThemeWithAlpha and DarkThemeWithoutAlpha is that WithAlpha will draw not selected items with halfe the 
                    // intensity instaed of solid "white" value
                    //
                    // Uncomment next line to use Dark Theme
                    // bottomBarPage.BarTheme = BottomBarPage.BarThemeTypes.DarkWithAlpha;

                    string[] tabTitles = { "Favorites", "Friends", "Nearby", "Recents", "Restaurants" };
                    string[] tabColors = { null, "#5D4037", "#7B1FA2", "#FF5252", "#FF9800" };
                    int[] tabBadgeCounts = { 0, 1, 5, 3, 4 };
                    string[] tabBadgeColors = { "#000000", "#FF0000", "#000000", "#000000", "#000000" };

                    for (int i = 0; i < tabTitles.Length; ++i)
                    {
                        string title = tabTitles[i];
                        string tabColor = tabColors[i];
                        int tabBadgeCount = tabBadgeCounts[i];
                        string tabBadgeColor = tabBadgeColors[i];


                        // create tab page
                        var tabPage = new HomePage()
                        {
                            Title = title,
                            //                    Icon = icon
                        };

                        // set tab color
                        if (tabColor != null)
                        {
                            BottomBarPageExtensions.SetTabColor(tabPage, Color.FromHex(tabColor));
                        }

                        // Set badges
                        //                BottomBarPageExtensions.SetBadgeCount(tabPage, tabBadgeCount);
                        //                BottomBarPageExtensions.SetBadgeColor(tabPage, Color.FromHex(tabBadgeColor));

                        // add tab pag to tab control
                        Children.Add(tabPage);
                    }#1#

                }

                protected override void OnCurrentPageChanged()
                {
                    base.OnCurrentPageChanged();
                }

                private FileImageSource GetIcon(string iconName)
                {
                    return (FileImageSource) ImageSource.FromFile($"menu_{iconName}.png");
                }*/
    }
}
