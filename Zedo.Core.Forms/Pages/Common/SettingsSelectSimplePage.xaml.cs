﻿using MvvmCross.Forms.Views;
using Xamarin.Forms;
using Zedo.Core.ViewModels;

namespace Zedo.Core.Forms.Pages
{
    public partial class SettingsSelectSimplePage : MvxContentPage<SettingsSelectSimpleViewModel>
    {
        public SettingsSelectSimplePage()
        {
            InitializeComponent();
        }

        protected override void OnViewModelSet()
        {
            base.OnViewModelSet();
        }

        private void ListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ViewModel.GoSelectItem.Execute(e.SelectedItem);
        }
    }
}
