﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using BottomBar.XamarinForms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Zedo.Core.Forms.Pages.Main;

namespace Zedo.Core.Forms.Pages.Common
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainPage : ContentPage
	{
	    public MainPage()
	    {
	        InitializeComponent();
        }
    }
}