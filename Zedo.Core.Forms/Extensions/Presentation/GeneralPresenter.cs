﻿using System;
using System.Collections.Generic;
using System.Linq;
using MvvmCross.Forms.Core;
using MvvmCross.Forms.Presenters;
using MvvmCross.Forms.Presenters.Attributes;
using MvvmCross.Forms.Views;
using MvvmCross.Presenters;
using MvvmCross.Presenters.Hints;
using MvvmCross.ViewModels;
using MvvmCross.Views;
using Xamarin.Forms;
using Zedo.Core.Forms.Pages;
using Zedo.Core.Forms.Pages.Account;
using Zedo.Core.Forms.Pages.Main;
using Zedo.Core.Forms.Pages.Portal.Home;

namespace Zedo.iOS.Extensions.Presentation
{
    public class GeneralPresenter : MvxFormsPagePresenter
    {
        public GeneralPresenter(IMvxFormsViewPresenter platformPresenter) : base(platformPresenter)
        {
        }

        private IReadOnlyList<Page> navigationModalStack;

        public override async void Show(MvxViewModelRequest request)
        {
            if (request.PresentationValues != null)
            {
                if (request.PresentationValues.ContainsKey("NavigationMode"))
                {
                    var presentationMode = request.PresentationValues["NavigationMode"];
                    if (presentationMode == "Modal")
                    {
                        var attr = new MvxModalPresentationAttribute();

                        var viewType = ViewsContainer.GetViewType(request.ViewModelType);

//                    var pageType = typeof(LoginPage);
                        var page = CloseAndCreatePage(viewType, request, attr, closeModal: false);
                        var tabbedPage = GetPageOfType<TabbedPage>();
                        if (tabbedPage != null)
                        {
                            tabbedPage.Navigation.PushModalAsync(CreateNavigationPage(page));
                            navigationModalStack = tabbedPage.Navigation.ModalStack;
                            var t = navigationModalStack.ToArray();
                        }
                        else
                        {
                            ShowModal(viewType, attr, request);// MasterNavigationController.PopToRootViewController(false);
                        }

                        return;
                        
                    }
                    else if (presentationMode == "ReplaceRoot")
                    {
                        var attribute = GetPresentationAttribute(request);
                        attribute.ViewModelType = request.ViewModelType;
                        var viewType = ViewsContainer.GetViewType(request.ViewModelType);
                        
                        var page = base.CloseAndCreatePage(viewType, request, (MvxPagePresentationAttribute)attribute);
                        base.PushOrReplacePage(null, page, (MvxPagePresentationAttribute)attribute);
                    }
                }
            }

            base.Show(request);
        }

        protected override Page CloseAndCreatePage(Type view, MvxViewModelRequest request, MvxPagePresentationAttribute attribute,
            bool closeModal = true, bool closePlatformViews = true, bool showPlatformViews = true)
        {
            if (closeModal && FormsApplication.MainPage?.Navigation.ModalStack.Any() == true)
            {
                closeModal = false;
            }

            return base.CloseAndCreatePage(view, request, attribute, closeModal, closePlatformViews, showPlatformViews);
        }

        public override void PushOrReplacePage(Page rootPage, Page page, MvxPagePresentationAttribute attribute)
        {
            // Make sure we always have a rootPage
            if (rootPage == null)
            {
                rootPage = FormsApplication.MainPage;
            }

            if (rootPage?.Navigation.ModalStack.LastOrDefault() is NavigationPage modalNavigation)
            {
                base.PushOrReplacePage(modalNavigation, page, attribute);
//                PushOrReplacePage(modalNavigation, page, attribute);
                return;
            }

            if (attribute.WrapInNavigationPage)
            {
                var tabbedPage = GetPageOfType<TabbedPage>(rootPage);
                if (tabbedPage != null)
                {
                    var IsSectionRoot = page is HomePage || page is FavoritesPage || page is NotificationsPage || page is AccountSectionPage;
                    NavigationPage navPage = null;
                    if (IsSectionRoot)
                    {
                        navPage = CreateNavigationPage(page);
                        navPage.Icon = attribute.Icon;
                        tabbedPage.Children.Add(navPage);
                        return;
                    }
                    if (tabbedPage.CurrentPage is NavigationPage)
                    {
                        navPage = tabbedPage.CurrentPage as NavigationPage;// CreateNavigationPage(page);
                        PushOrReplacePage(navPage, page, attribute);
                        return;
                    }
                }
            }

            base.PushOrReplacePage(rootPage, page, attribute);
        }

        public override void ChangePresentation(MvxPresentationHint hint)
        {
            if (hint is MvxClosePresentationHint closePresentationHint)
            {
//                var modalViewModel = closePresentationHint.ViewModelToClose as IModalViewModel;
//                if (modalViewModel != null && modalViewModel.IsModal)
//                {
                    FormsApplication.MainPage.Navigation.PopModalAsync(true);

                    return;
//                }
            }
            base.ChangePresentation(hint);
        }

        public override TPage GetPageOfType<TPage>(Page rootPage = null)
        {
            if (rootPage == null)
                rootPage = FormsApplication.MainPage;

            if (rootPage is TPage)
            {
                return rootPage as TPage;
            }
            else if (rootPage is TabbedPage tabbedPage)
            {
                if (tabbedPage.CurrentPage is TPage currentTab)
                {
                    return currentTab;
                }
                else
                {
                    return base.GetPageOfType<TPage>(tabbedPage.CurrentPage);
                }
            }

            return base.GetPageOfType<TPage>(rootPage);
        }

        public override bool CloseContentPage(IMvxViewModel viewModel, MvxContentPagePresentationAttribute attribute)
        {
            var appRoot = FormsApplication.MainPage;
            if (appRoot.Navigation.ModalStack?.FirstOrDefault()?.Navigation?.NavigationStack.Count > 0)
            {
                appRoot.Navigation.ModalStack[0].Navigation.PopAsync();
//                if (ClosePage(appRoot.Navigation.ModalStack.Last(), null, attribute))
//                {
                    return true;
//                }
            }

            return base.CloseContentPage(viewModel, attribute);
        }

        protected override bool ClosePage(Page rootPage, Page page, MvxPagePresentationAttribute attribute)
        {
            return base.ClosePage(rootPage, page, attribute);
        }
    }
}