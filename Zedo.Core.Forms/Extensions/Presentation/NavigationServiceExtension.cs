﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;

namespace Zedo.iOS.Extensions.Presentation
{
    public static class NavigationServiceExtension
    {
        public static Task NavigateWithMode<T>(this IMvxNavigationService service, PresentationNavigationModes mode) where T : IMvxViewModel
        {
            MvxBundle presentation = null;
            if (mode == PresentationNavigationModes.ReplaceRoot)
            {
                presentation = new MvxBundle(new Dictionary<string, string>() { { "NavigationMode", "ReplaceRoot" } });
            }

            return service.Navigate<T>(presentation);
        }
    }
}