﻿//using BottomBar.XamarinForms;
using MvvmCross;
using MvvmCross.Forms.Core;
using MvvmCross.Plugin.Messenger;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using Zedo.Core.Forms.Pages;
using Zedo.Core.Forms.Pages.Main;
using Zedo.Core.Messages;

namespace Zedo.Core.Forms
{
    public partial class App : MvxFormsApplication
    {
        private MvxSubscriptionToken _onRootPageSubscription;

        public App()
        {
            Current.On<Xamarin.Forms.PlatformConfiguration.Android>()
                .UseWindowSoftInputModeAdjust(WindowSoftInputModeAdjust.Resize);

            InitializeComponent();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            var _messenger = Mvx.Resolve<IMvxMessenger>();
            _onRootPageSubscription = _messenger.SubscribeOnMainThread<RootPageRequestMessage>(message => ShowPortalPage());
       }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        public void ShowPortalPage()
        {
//            var bottomBarPage = new BottomBarPage();

            var rootPage = new HomeRootPage();
            MainPage = rootPage;
        }
    }
}
